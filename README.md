# GeneaLogic Client

GeneaLogic Client is the client module of an open-source GeneaLogic application that also includes [GeneaLogic API](https://gitlab.com/praglis/genealogic-api). GeneaLogic is a genealogical application created to aid genealogists with their work.
The application was created by Paweł Raglis within the engineer's thesis 'Family tree maker' from the Faculty of Computer Science on Białystok University of Technology and published as an open-source project on 14.02.2021.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8.

## Angular installation

To install the Angular framework, you need to [download](https://www.npmjs.com/get-npm) and install the NPM. The next step is an installation of Angular using the `npm install -g angular-cli` command.

After successful installation of the framework, you should navigate to the root folder of GeneaLogic Client, and then install all the required dependencies with the `npm install` command.

## Running the module

The last step is to run the client module. Run `ng serve` for a development server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

To obtain a fully working GeneaLogic application, you need to launch the other module — [GeneaLogic API](https://gitlab.com/praglis/genealogic-api) at the same time.

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md). More info on Angular on [the official website](https://angular.io/).
