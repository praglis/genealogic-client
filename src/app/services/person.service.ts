import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {Relationship} from '../interfaces/api/enums/relationship';
import {Person} from '../interfaces/api/models/person';
import {Pageable} from '../interfaces/api/other/pageable';
import {EventModel} from '../interfaces/api/models/event-model';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  API_URL = 'http://localhost:8080/persons';

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) {
  }

  create(person: Person) {
    return this.httpClient.post(this.API_URL, person, this.userService.getRequestOptionsWithAuthorization());
  }

  createRelative(person: Person, relationship: Relationship, relativeId: number, familyId?: number) {
    if (familyId == null) {
      return this.httpClient.post(`${this.API_URL}/${relativeId}/${Relationship[relationship]}`, person,
        this.userService.getRequestOptionsWithAuthorization());
    } else {
      return this.httpClient.post(`${this.API_URL}/${relativeId}/family/${familyId}/${Relationship[relationship]}`, person,
        this.userService.getRequestOptionsWithAuthorization());
    }
  }

  getTreePersons(treeId: number, page?: Pageable<Person>): Observable<Pageable<Person>> {
    if (page != null) {
      return this.httpClient.get<Pageable<Person>>(`${this.API_URL}/tree/${treeId}${page.buildPageableUrl()}`,
        this.userService.getRequestOptionsWithAuthorization());
    }
    return this.httpClient.get<Pageable<Person>>(`${this.API_URL}/tree/${treeId}?size=10000`,
      this.userService.getRequestOptionsWithAuthorization());
  }

  get(id: number): Observable<Person> {
    return this.httpClient.get<Person>(this.API_URL + '/' + id, this.userService.getRequestOptionsWithAuthorization());
  }

  update(person: Person) {
    return this.httpClient.put(this.API_URL + '/' + person.id, person, this.userService.getRequestOptionsWithAuthorization());
  }

  delete(id: number) {
    return this.httpClient.delete(this.API_URL + '/' + id, this.userService.getRequestOptionsWithAuthorization());
  }


  // getPersonEvents(personId: number): Observable<Pageable<EventModel>> {
  //   return this.httpClient.get<Pageable<EventModel>>(`${this.API_URL}/${personId}/events`,
  //     this.userService.getRequestOptionsWithAuthorization());
  // }

  getTimelineForPerson(personId: number, pageable: Pageable<EventModel>): Observable<Pageable<EventModel>> {
    return this.httpClient.get<Pageable<EventModel>>(`${this.API_URL}/${personId}/timeline${pageable.buildPageableUrl()}`,
      this.userService.getRequestOptionsWithAuthorization());
  }
}

