import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarRef, SimpleSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  constructor(private snackBar: MatSnackBar) {
  }

  displaySuccess(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, null, {panelClass: ['bg-success']});
  }

  displayDismissibleSuccess(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, 'Dismiss', {panelClass: ['bg-success']});
  }

  displayDismissibleInfo(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, 'Dismiss', {panelClass: ['bg-primary'], duration: undefined});
    // todo check if 'duration: undefined' is really needed here
  }

  displayError(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, null, {panelClass: ['bg-danger']});
  }

  displayDismissibleError(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, 'Dismiss', {panelClass: ['bg-danger']});
  }

  displayWarning(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, null, {panelClass: 'bg-warning'});
  }

  displayDismissibleWarning(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackBar.open(message, 'Dismiss', {panelClass: 'bg-warning', duration: undefined});
    // todo check if 'duration: undefined' is really needed here
  }
}
