import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {Observable} from 'rxjs';
import {BasicEvents} from '../interfaces/api/other/basic-events';
import {EventModel} from '../interfaces/api/models/event-model';
import {EventType} from '../interfaces/api/enums/event-type';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  API_URL = 'http://localhost:8080/events';

  constructor(private httpClient: HttpClient,
              private userService: UserService) {
  }

  public static isEventTypePersonal(type: EventType): boolean {
    return EventType[type] !== EventType.DIV && EventType[type] !== EventType.MARR && EventType[type] !== EventType.ENGA;
  }

  get(id: number): Observable<EventModel> {
    return this.httpClient.get<EventModel>(`${this.API_URL}/${id}`, this.userService.getRequestOptionsWithAuthorization());
  }

  getLifespanEvents(personId: number): Observable<EventModel[]> {
    return this.httpClient.get<EventModel[]>(`${this.API_URL}/lifespans/person/${personId}`,
      this.userService.getRequestOptionsWithAuthorization());
  }

  getBasicByPerson(personId: number): Observable<BasicEvents> {
    return this.httpClient.get<BasicEvents>(`${this.API_URL}/basic/person/${personId}`,
      this.userService.getRequestOptionsWithAuthorization());
  }

  create(event: EventModel): Observable<EventModel> {
    return this.httpClient.post<EventModel>(this.API_URL, event, this.userService.getRequestOptionsWithAuthorization());
  }

  update(event: EventModel): Observable<EventModel> {
    return this.httpClient.put<EventModel>(this.API_URL + '/' + event.id, event, this.userService.getRequestOptionsWithAuthorization());
  }

  deleteIfExists(id: number) {
    if (id === undefined) {
      return;
    }
    return this.delete(id);
  }

  delete(id: number) {
    return this.httpClient.delete(this.API_URL + '/' + id, this.userService.getRequestOptionsWithAuthorization());
  }
}
