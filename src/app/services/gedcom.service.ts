import {Injectable} from '@angular/core';
import {UserService} from './user.service';
import {Observable} from 'rxjs';
import {SupportedGedcomVersion} from '../interfaces/api/enums/supported-gedcom-version.enum';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GedcomService {

  API_URL = 'http://localhost:8080/gedcom';

  constructor(private httpClient: HttpClient,
              private userService: UserService) {
  }

  importGedcom(gedcom: File) {
    const formData: FormData = new FormData();
    formData.append('file', gedcom);
    return this.httpClient.post(this.API_URL, formData, this.userService.getRequestOptionsWithAuthorization());
  }

  exportGedcom(treeId: number, version: SupportedGedcomVersion): Observable<any> {
    const httpOptions: Object = {
      headers: {
        ['Authorization']: 'Bearer ' + this.userService.currentlyLoggedUser.token,
      },
      withCredentials: true,
      responseType: 'text'
    };
    return this.httpClient.get<any>(`${this.API_URL}/${SupportedGedcomVersion[version]}/tree/${treeId}`, httpOptions);
  }
}
