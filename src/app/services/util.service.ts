import {Injectable} from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  public static APP_NAME = 'GeneaLogic';

  constructor() {
  }

  public static convertEmptyStringToNull(str: string): string {
    return str === '' ? null : str;
  }

  public static prepareTooLongErrorMessage(control: AbstractControl) {
    return `Too long (maximum is ${control.validator(control).maxlength.requiredLength} characters).`;
  }
}
