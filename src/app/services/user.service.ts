import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';
import {map} from 'rxjs/operators';
import {RegistrationForm} from '../interfaces/api/other/registration-form';
import {LoginForm} from '../interfaces/api/other/login-form';
import {User} from '../interfaces/api/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
  }

  public get currentUsername() {
    return this.currentUserSubject.value.username;
  }

  public get currentlyLoggedUser(): User {
    return this.currentUserSubject.value;
  }

  // public currentUser: Observable<any>;
  private API_URL = 'http://localhost:8080/users';
  private currentUserSubject: BehaviorSubject<any>;

  register(registrationForm: RegistrationForm) {
    return this.http.post(this.API_URL, registrationForm, {withCredentials: true});
  }

  login(loginForm: LoginForm) {
    return this.http.post<any>(this.API_URL + `/auth`, loginForm, {withCredentials: true})
      .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  getRequestOptionsWithAuthorization() {
    return {
      headers: {
        ['Authorization']: 'Bearer ' + this.currentlyLoggedUser.token,
      },
      withCredentials: true
    };
  }
}
