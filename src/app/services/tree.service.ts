import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {Pageable} from '../interfaces/api/other/pageable';
import {Tree} from '../interfaces/api/models/tree';
import {EventModel} from '../interfaces/api/models/event-model';
import {User} from '../interfaces/api/models/user';

@Injectable()
export class TreeService {
  private activeTreeSubject: BehaviorSubject<any>;

  API_URL = 'http://localhost:8080/trees';

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) {
    this.activeTreeSubject = null;
  }

  public get activeTree(): Tree {
    return this.activeTreeSubject?.value;
  }

  public set activeTree(activeTree: Tree) {
    this.activeTreeSubject = new BehaviorSubject<Tree>(activeTree);
  }

  public reloadActiveTree() {
    this.getById(this.activeTree.id).subscribe(response => this.activeTree = response);
  }

  createTree(tree: Tree) {
    return this.httpClient.post(this.API_URL, tree, this.userService.getRequestOptionsWithAuthorization());
  }

  getById(id: number): Observable<Tree> {
    return this.httpClient.get<Tree>(this.API_URL + '/' + id, this.userService.getRequestOptionsWithAuthorization());
  }

  getUserTrees(id: number): Observable<Pageable<Tree>> {
    return this.httpClient.get<Pageable<Tree>>(this.API_URL + '/owner/' + id, this.userService.getRequestOptionsWithAuthorization());
  }

  getTimelineForTree(treeId: number, pageable: Pageable<EventModel>): Observable<Pageable<EventModel>> {
    return this.httpClient.get<Pageable<EventModel>>(`${this.API_URL}/${treeId}/timeline${pageable.buildPageableUrl()}`,
      this.userService.getRequestOptionsWithAuthorization());
  }

  deleteTree(id: number) {
    return this.httpClient.delete(this.API_URL + '/' + id, this.userService.getRequestOptionsWithAuthorization());
  }

  modifyTree(tree: Tree) {
    return this.httpClient.put(this.API_URL + '/' + tree.id, tree, this.userService.getRequestOptionsWithAuthorization());
  }

  modifyProband(treeId: number, personId: number) {
    return this.httpClient.patch(this.API_URL + `/${treeId}/proband-id`, personId, this.userService.getRequestOptionsWithAuthorization());
  }

  getPermittedUsersForTree(treeId: number): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.API_URL}/${treeId}/permitted-users`,
      this.userService.getRequestOptionsWithAuthorization());
  }

  grantPermission(treeId: number, username: string): Observable<User[]> {
    return this.httpClient.post<User[]>(`${this.API_URL}/${treeId}/permitted-users`, username,
      this.userService.getRequestOptionsWithAuthorization());
  }

  revokePermission(treeId: number, userId: number): Observable<User[]> {
    return this.httpClient.delete<User[]>(`${this.API_URL}/${treeId}/permitted-users/${userId}`,
      this.userService.getRequestOptionsWithAuthorization());
  }
}

