import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {EventParticipant} from '../interfaces/api/models/event-participant';

@Injectable({
  providedIn: 'root'
})
export class EventParticipantService {

  API_URL = 'http://localhost:8080/event-participants';

  constructor(private httpClient: HttpClient,
              private userService: UserService) {
  }

  create(participant: EventParticipant) {
    return this.httpClient.post(this.API_URL, participant, this.userService.getRequestOptionsWithAuthorization());
  }
}
