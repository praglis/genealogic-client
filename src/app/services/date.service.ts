import {Injectable} from '@angular/core';
import {DateEstimation} from '../interfaces/api/enums/date.estimation';
import {EventModel} from '../interfaces/api/models/event-model';
import {DateAccuracy} from '../interfaces/api/enums/date-accuracy.enum';
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor(private datePipe: DatePipe) {
  }

  static concatWithEstimationString(estimation: DateEstimation, value: string, valueMax: string, phrase: string) {
    if (value == null) {
      return 'unknown';
    }
    switch (DateEstimation[estimation] as unknown as number) {
      case DateEstimation.EXACT:
        return value;
      case DateEstimation.APPROXIMATED:
        return 'circa ' + value;
      case DateEstimation.AFTER:
        return 'after ' + value;
      case DateEstimation.BEFORE:
        return 'before ' + value;
      case DateEstimation.BETWEEN:
        return `between ${value} and ${valueMax}`;
      case DateEstimation.UNKNOWN:
        return phrase;
      default:
        return '??';
    }
  }

  public getEventDate(event: EventModel) {
    if (event?.startDate == null) {
      return 'unknown';
    }
    return DateService.concatWithEstimationString(
      event.startDate.estimation,
      this.convertDateToString(event.startDate.value, event.startDate.valueAccuracy, event.startDate.phrase),
      this.convertDateToString(event.startDate.maxValue, event.startDate.maxValueAccuracy, event.startDate.phrase),
      event.startDate.phrase
    );
  }

  convertDateToString(value: Date, accuracy: DateAccuracy, otherPhrase: string): string {
    if (value == null) {
      return otherPhrase;
    }
    switch (DateAccuracy[accuracy]) {
      // @ts-ignore
      case DateAccuracy.YEAR:
        return this.datePipe.transform(value, 'yyyy', null);
      // @ts-ignore
      case DateAccuracy.MONTH:
        return this.datePipe.transform(value, 'MM.yyyy', null);
      // @ts-ignore
      case DateAccuracy.DAY:
        return this.datePipe.transform(value, 'dd.MM.yyyy', null);
    }
  }
}
