import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {FileModel} from '../interfaces/api/other/file-model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  static defaultPhotoEncodedInBase64: string;

  constructor(private httpClient: HttpClient) {
    this.getDefaultPhoto().subscribe(value => this.encodeFileInBase64(value).subscribe(
      encodedFile => FileService.defaultPhotoEncodedInBase64 = 'data:image/jpg;base64,' + encodedFile.encodedBytes
    ));
  }

  encodeFileInBase64(photo: File): Observable<FileModel> {
    if (photo == null) {
      return of(null);
    }
    const reader = new FileReader();
    reader.readAsDataURL(photo);
    return new Observable<FileModel>((observer) => {
      let encodedFile: FileModel;
      reader.onload = () => {
        const readerResult = reader.result as string;
        encodedFile = {encodedBytes: readerResult.substring(readerResult.indexOf(',') + 1), fileName: photo.name};
        observer.next(encodedFile);
        observer.complete();
      };
      return {
        unsubscribe() {
          encodedFile = null;
        }
      };
    });
  }

  getDefaultPhoto(): Observable<File> {
    return this.httpClient.get<File>('assets/default-photo.svg');
  }
}
