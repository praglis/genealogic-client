import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {Pageable} from '../interfaces/api/other/pageable';
import {Family} from '../interfaces/api/models/family';

@Injectable({
  providedIn: 'root'
})
export class FamilyService {
  API_URL = 'http://localhost:8080/families';

  constructor(
    private httpClient: HttpClient,
    private userService: UserService
  ) {
  }

  getFamiliesContainingChild(childId: number): Observable<Pageable<Family>> {
    return this.httpClient.get<Pageable<Family>>(`${this.API_URL}/child/${childId}`,
      this.userService.getRequestOptionsWithAuthorization());
  }

  getFamiliesContainingSpouse(spouseId: number): Observable<Pageable<Family>> {
    return this.httpClient.get<Pageable<Family>>(`${this.API_URL}/spouse/${spouseId}`,
      this.userService.getRequestOptionsWithAuthorization());
  }

  getTreeFamilies(treeId: number): Observable<Pageable<Family>> {
    return this.httpClient.get<Pageable<Family>>(`${this.API_URL}/tree/${treeId}?size=10000`,
      this.userService.getRequestOptionsWithAuthorization());
  }
}
