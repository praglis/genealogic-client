export enum EventType {
  ADOP = 'Adoption',

  BIRT = 'Birth',
  BAPM = 'Baptism',
  BURI = 'Burial',

  CHR = 'Christening',
  CONF = 'Confirmation',

  DIV = 'Divorce',
  DEAT = 'Death',

  ENGA = 'Engagement',
  EMIG = 'Emigration',

  GRAD = 'Graduation',

  IMMI = 'Immigration',

  // MIGR = 'Migration', // included as IMMI or EMIG in GEDCOM
  MARR = 'Marriage',

  NATU = 'Naturalization',

  RETI = 'Retirement'
}
