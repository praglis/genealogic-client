export enum DateAccuracy {
  YEAR,
  MONTH,
  DAY
}
