export enum DateEstimation {
  EXACT,
  APPROXIMATED,
  BEFORE,
  AFTER,
  BETWEEN,
  UNKNOWN
}
