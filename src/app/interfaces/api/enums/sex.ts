export enum Sex {
  M = 'MALE',
  F = 'FEMALE',
  U = 'UNDETERMINED'
}
