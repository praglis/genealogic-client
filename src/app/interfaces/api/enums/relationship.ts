export enum Relationship {
  FATHER,
  MOTHER,
  CHILD,
  SPOUSE
}
