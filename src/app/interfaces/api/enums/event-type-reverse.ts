export enum EventTypeReverse {
  Adoption = 'ADOP',

  Birth = 'BIRT',
  Baptism = 'BAPM',
  Burial = 'BURI',

  Christening = 'CHR',
  Confirmation = 'CONF',

  Death = 'DEAT',
  Divorce = 'DIV',

  Engagement = 'ENGA',
  Emigration = 'EMIG',

  Graduation = 'GRAD',

  Immigration = 'IMMI',

  Marriage = 'MARR',

  Naturalization = 'NATU',

  Retirement = 'RETI'
}
