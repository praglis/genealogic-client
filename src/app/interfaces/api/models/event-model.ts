import {Place} from './place';
import {DateModel} from './date-model';
import {AbstractModel} from './abstract-model';
import {EventParticipant} from './event-participant';

export class EventModel implements AbstractModel {
  id: number;
  name: string;
  isNameCustom: boolean;
  type: any;
  place: Place;
  startDate: DateModel;
  cause: string;
  details: string;
  owner: any;
  participants: EventParticipant[];
}
