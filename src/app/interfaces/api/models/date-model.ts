import {DateEstimation} from '../enums/date.estimation';
import {DateAccuracy} from '../enums/date-accuracy.enum';

export class DateModel {
  value: Date;
  valueAccuracy: DateAccuracy;

  maxValue: Date;
  maxValueAccuracy: DateAccuracy;

  estimation: DateEstimation;

  phrase: string;


  constructor(estimation: DateEstimation, value: Date, valueAccuracy: DateAccuracy, maxValue?: Date, maxValueAccuracy?: DateAccuracy,
              phrase?: string) {
    this.value = value;
    this.valueAccuracy = valueAccuracy;
    this.maxValue = maxValue;
    this.maxValueAccuracy = maxValueAccuracy;
    this.estimation = estimation;
    this.phrase = phrase;
  }

  static getYear(dateModel: DateModel) {
    if (dateModel == null) {
      return '?';
    }
    const fullYear = new Date(dateModel.value).getFullYear();
    switch (DateEstimation[dateModel.estimation] as unknown as number) {
      case DateEstimation.EXACT:
        return fullYear;
      case DateEstimation.APPROXIMATED:
        return 'circa ' + fullYear;
      case DateEstimation.AFTER:
        return 'after ' + fullYear;
      case DateEstimation.BEFORE:
        return 'before ' + fullYear;
      case DateEstimation.BETWEEN:
        const maxFullYear = new Date(dateModel.maxValue).getFullYear();
        if (fullYear === maxFullYear) {
          return fullYear;
        } else {
          return 'circa ' + Math.ceil(((fullYear + maxFullYear) / 2));
        }
      case DateEstimation.UNKNOWN:
        return dateModel.phrase;
      default:
        return '??';
    }
  }
}
