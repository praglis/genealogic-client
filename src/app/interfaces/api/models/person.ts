import {Family} from './family';
import {Place} from './place';
import {AbstractModel} from './abstract-model';
import {Tree} from './tree';
import {Sex} from '../enums/sex';
import {FileModel} from '../other/file-model';


export class Person implements AbstractModel {
  id: number;
  tree: Tree;

  namePrefix: string;
  firstName: string;
  middleName: string;
  nickname: string;
  surnamePrefix: string;
  lastName: string;
  maidenName: string;
  nameSuffix: string;

  parentFamilies: Family[];
  wifeFamilies: Family[];
  husbandFamilies: Family[];

  sex: Sex;
  residency: Place;
  occupation: string;
  bio: string;
  photo: FileModel;

  static of(id: number, tree: Tree, namePrefix: string, firstName: string, middleName: string, nickname: string,
            surnamePrefix: string, lastName: string, maidenName: string, nameSuffix: string, sex: Sex, residency: Place,
            occupation: string, bio: string, photo: FileModel): Person {
    const person: Person = new Person();
    person.id = id;
    person.tree = tree;
    person.namePrefix = namePrefix;
    person.firstName = firstName;
    person.middleName = middleName;
    person.nickname = nickname;
    person.surnamePrefix = surnamePrefix;
    person.lastName = lastName;
    person.maidenName = maidenName;
    person.nameSuffix = nameSuffix;
    person.sex = sex;
    person.residency = residency;
    person.occupation = occupation;
    person.bio = bio;
    person.photo = photo;
    return person;
  }

  static fullName(person: Person): string {
    if (person == null) {
      return 'unknown';
    }
    const namePrefix = person.namePrefix != null ? `${person.namePrefix} ` : '';
    const firstName = person.firstName != null ? `${person.firstName} ` : '';
    const middleName = person.middleName != null ? `${person.middleName} ` : '';
    const nickname = person.nickname != null ? `'${person.nickname}' ` : '';

    const surnamePrefix = person.surnamePrefix != null ? `${person.surnamePrefix} ` : '';
    const lastName = person.lastName != null ? `${person.lastName} ` : '';
    const maidenName = person.maidenName != null ? `(${person.maidenName}) ` : '';
    const nameSuffix = person.nameSuffix != null ? `${person.nameSuffix} ` : '';
    return `${namePrefix}${firstName}${middleName}${nickname}${surnamePrefix}${lastName}${maidenName}${nameSuffix}`.slice(0, -1);
  }
}
