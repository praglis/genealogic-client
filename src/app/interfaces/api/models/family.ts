import {Person} from './person';
import {AbstractModel} from './abstract-model';

export class Family implements AbstractModel {
  id: number;

  husband: Person;

  wife: Person;

  children: Person[];

  details: string;

  static of(): Family {
    return new Family();
  }

  static hasExactlyOneSpouse(family: Family): boolean {
    return family?.husband != null && family?.wife == null || family?.wife != null && family?.husband == null;
  }

  public static toString(family: Family): string {
    let spousesString = '';
    if (family.husband != null || family.wife != null) {
      spousesString = 'spouses: ';
      let husbandString = '';
      let wifeString = ' and ';
      if (family.husband != null) {
        husbandString = Person.fullName(family.husband);
      } else {
        wifeString = '';
      }
      if (family.wife != null) {
        wifeString = wifeString.concat(Person.fullName(family.wife));
      } else {
        wifeString = '';
      }
      spousesString = spousesString.concat(husbandString + wifeString + '; ');
    }

    let childrenString = '';
    if (family.children != null) {
      childrenString = 'children:';
      for (const child of family.children) {
        childrenString = childrenString.concat(` ${Person.fullName(child)},`);
      }
      childrenString = childrenString.slice(0, -1);
    } else {
      spousesString = spousesString.slice(0, -2);
    }
    return spousesString + childrenString;
  }

  static getOtherSpouse(family: Family, spouse: Person): Person {
    return family?.wife?.id === spouse.id ? family?.husband : family?.wife;
  }
}
