export interface User {
  id: number;
  token: string;
  username: string;
  firstName: string;
  lastName: string;
}
