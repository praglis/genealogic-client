import {AbstractModel} from './abstract-model';

export class Place implements AbstractModel {
  id: number;

  name: string;
  address: string;

  city: string;
  state: string;
  postalCode: string;
  country: string;

  public static firstPart(placeName: string, other = 'unknown place') {
    if (placeName == null) {
      return other;
    }
    return placeName.includes(',') ? placeName.substring(0, placeName.indexOf(',')) : placeName;
  }

  public static getFullPlaceString(place: Place, other = 'unknown place'): string {
    if (place == null) {
      return other;
    }
    const name = place.name != null ? `${place.name}` : '';
    const city = place.city != null ? `\nCity: ${place.city}` : '';
    const state = place.state != null ? `\nState: ${place.state}` : '';
    const postalCode = place.postalCode != null ? `\nPostal code: ${place.postalCode}` : '';
    const country = place.country != null ? `\nCountry: ${place.country}` : '';
    const address = place.address != null ? `\nAddress: ${place.address}` : '';

    return `${name}${city}${state}${postalCode}${country}${address}`;
  }
}
