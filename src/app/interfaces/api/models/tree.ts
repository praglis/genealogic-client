export class Tree {
  id: number;
  creationDate: Date;
  updateDate: Date;
  owner: any;
  probandId: number;
  name: string;
  personsCount: number;
}


