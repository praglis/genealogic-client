export class EventRole {
  descriptor: string;

  constructor(descriptor: string) {
    this.descriptor = descriptor;
  }
}
