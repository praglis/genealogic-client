import {Person} from './person';
import {EventModel} from './event-model';
import {EventRole} from './event-role';

export class EventParticipant {
  person: Person;
  event: EventModel;
  role: EventRole;

  constructor(person: Person, event: EventModel, roleDescriptor: string) {
    this.person = person;
    this.event = event;
    this.role = new EventRole(roleDescriptor);
  }
}
