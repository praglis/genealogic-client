export class Pageable<T> {
  content: T[];
  isSorted: boolean;
  sortedBy: string;
  dir: string;
  size: number;
  number: number;
  totalElements: number;
  totalPages: number;

  // static of(page = 0, size = 20): Pageable {
  //   return new Pageable(null, 0, size, page);
  // }

  public buildPageableUrl(): string {
    let url = '/?';
    if (this.sortedBy != null) {
      url += 'sort=' + this.sortedBy + '&';
    }
    if (this.dir != null) {
      url += 'dir=' + this.dir + '&';
    }
    if (this.size != null) {
      url += 'size=' + this.size + '&';
    }
    if (this.number != null) {
      url += 'page=' + this.number;
    }
    return url;
  }

  fromResponse(response: any) {
    this.content = response.content;
    this.size = response.size;
    this.number = response.number;
    this.isSorted = response.sort;
    this.totalElements = response.totalElements;
    this.totalPages = response.totalPages;
    this.dir = response.dir;
  }
}
