export interface RegistrationForm {
  username: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  role: string;
}
