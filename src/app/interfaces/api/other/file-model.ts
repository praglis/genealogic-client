export class FileModel {
  fileName: string;
  encodedBytes: string;

  static dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    return new Blob([int8Array], {type: 'image/png'});
  }

  static toFile(fileModel: FileModel) {
    if (fileModel?.encodedBytes != null) {
      return new File([FileModel.dataURItoBlob(fileModel.encodedBytes)], fileModel.fileName, {type: 'image/png'});
    } else {
      return null;
    }
  }


}
