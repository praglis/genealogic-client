import {EventModel} from '../models/event-model';

export interface BasicEvents {
  birth: EventModel;
  death: EventModel;
  burial: EventModel;
}
