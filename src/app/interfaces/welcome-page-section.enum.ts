export enum WelcomePageSectionEnum {
  NAVBAR,
  TREES,
  TREE_VIEWS,
  TIMELINES,
  PERSONS_PROFILE
}
