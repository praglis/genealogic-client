import {DateModel} from '../api/models/date-model';
import {Place} from '../api/models/place';

export class Lifespan {
  birthDate: DateModel;
  birthPlace: Place;
  deathDate: DateModel;
  deathOrResidencyPlace: Place;
  isPersonAlive: boolean;

  constructor(birthDate: DateModel, birthPlace: Place) {
    this.birthDate = birthDate;
    this.birthPlace = birthPlace;
  }

  public getCleanBirthYear() {
    if (this?.birthDate != null) {
      return DateModel.getYear(this.birthDate);
    } else {
      return '???';
    }
  }

  public getCleanDeathYear() {
    if (this?.deathDate != null) {
      return DateModel.getYear(this.deathDate);
    } else {
      return '???';
    }
  }

  public getCleanBirthPlace() {
    if (this?.birthPlace != null) {
      return Place.firstPart(this.birthPlace.name);
    } else {
      return 'unknown';
    }
  }

  public getCleanDeathOrResiPlace() {
    if (this?.deathOrResidencyPlace != null) {
      return Place.firstPart(this.deathOrResidencyPlace.name);
    } else {
      return 'unknown';
    }
  }

  public getCleanLifespan(): string {
    const deathDateOrLiving = this.isPersonAlive ? 'living' : this.getCleanDeathYear();
    return `${this.getCleanBirthYear()}, ${this.getCleanBirthPlace()} - ${deathDateOrLiving}, ${this.getCleanDeathOrResiPlace()}`;
  }
}

