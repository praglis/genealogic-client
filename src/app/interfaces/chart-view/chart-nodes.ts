import {Person} from '../api/models/person';
import {Lifespan} from './lifespan';
import {FileService} from '../../services/file.service';

export class TreeNode {
  id: number;
  fullName: string;
  birth: string;
  deathOrResidency: string;
  photoUrl: string;

  constructor(person: Person, lifespan: Lifespan) {
    this.id = person.id;
    this.fullName = Person.fullName(person);
    this.birth = `* ${lifespan.getCleanBirthYear()}, ${lifespan.getCleanBirthPlace()}`;
    if (lifespan.isPersonAlive === true) {
      this.deathOrResidency = `living, ${lifespan.getCleanDeathOrResiPlace()}`;
    } else {
      this.deathOrResidency = `+ ${lifespan.getCleanDeathYear()}, ${lifespan.getCleanDeathOrResiPlace()}`;
    }
    this.photoUrl = person?.photo?.encodedBytes != null
      ? 'data:image/jpg;base64,' + person?.photo?.encodedBytes : FileService.defaultPhotoEncodedInBase64;
  }
}

export class OneParentNode extends TreeNode {
  pid: number;

  constructor(person: Person, pid: number, lifespan: Lifespan) {
    super(person, lifespan);
    this.pid = pid;
  }
}

export class PartnerNode extends TreeNode {
  pid: number;
  tags;

  constructor(person: Person, pid: number, lifespan: Lifespan) {
    super(person, lifespan);
    this.pid = pid;
    this.tags = ['partner'];
  }
}

export class TwoParentNode extends OneParentNode {
  ppid: number;

  constructor(person: Person, pid: number, ppid: number, lifespan: Lifespan) {
    super(person, pid, lifespan);
    this.ppid = ppid;
  }
}
