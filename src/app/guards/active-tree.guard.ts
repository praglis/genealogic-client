import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {TreeService} from '../services/tree.service';

@Injectable({
  providedIn: 'root'
})
export class ActiveTreeGuard implements CanActivate {

  constructor(private treeService: TreeService,
              private router: Router
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.treeService.activeTree == null) {
      this.router.navigate(['/trees']);
      return false;
    } else {
      return true;
    }
  }

}
