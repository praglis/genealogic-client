import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TreesComponent} from './components/trees/trees.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {LoggedInGuard} from './guards/logged-in.guard';
import {ListViewComponent} from './components/list-view/list-view.component';
import {PersonEditComponent} from './components/person-edit/person-edit.component';
import {ActiveTreeGuard} from './guards/active-tree.guard';
import {ChartViewComponent} from './components/chart-view/chart-view.component';
import {PersonProfileComponent} from './components/person-profile/person-profile.component';
import {WelcomePageComponent} from './components/welcome-page/welcome-page.component';
import {StartingPageComponent} from './components/starting-page/starting-page.component';
import {EventDetailsComponent} from './components/event-details/event-details.component';
import {TimelinePersonComponent} from './components/timeline/timeline-person.component';
import {TimelineTreeComponent} from './components/timeline/timeline-tree.component';
import {EventCreateComponent} from './components/event-create-edit/event-create.component';
import {EventEditComponent} from './components/event-create-edit/event-edit.component';
import {PermissionComponent} from './components/permission-edit/permission.component';


const routes: Routes = [
  {path: '', component: StartingPageComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'about', component: WelcomePageComponent, canActivate: [LoggedInGuard]},
  {path: 'trees', component: TreesComponent, canActivate: [LoggedInGuard]},
  {path: 'view/list', component: ListViewComponent, canActivate: [LoggedInGuard, ActiveTreeGuard]},
  {path: 'view/chart/:id', component: ChartViewComponent, canActivate: [LoggedInGuard, ActiveTreeGuard]},
  {path: 'view/chart', component: ChartViewComponent, canActivate: [LoggedInGuard, ActiveTreeGuard]},
  {path: 'tree/timeline', component: TimelineTreeComponent, canActivate: [LoggedInGuard, ActiveTreeGuard]},
  {path: 'tree/permissions', component: PermissionComponent, canActivate: [LoggedInGuard, ActiveTreeGuard]},
  {path: 'person/new/:relation/of/:relativeId', component: PersonEditComponent, canActivate: [LoggedInGuard]},
  {path: 'person/new', component: PersonEditComponent, canActivate: [LoggedInGuard, ActiveTreeGuard]},
  {path: 'person/:id/update', component: PersonEditComponent, canActivate: [LoggedInGuard]},
  {path: 'person/:id/timeline', component: TimelinePersonComponent, canActivate: [LoggedInGuard]},
  {path: 'person/:id', component: PersonProfileComponent, canActivate: [LoggedInGuard]},
  {path: 'event/new', component: EventCreateComponent, canActivate: [LoggedInGuard, ActiveTreeGuard]},
  {path: 'event/:id', component: EventDetailsComponent, canActivate: [LoggedInGuard]},
  {path: 'event/:id/update', component: EventEditComponent, canActivate: [LoggedInGuard]},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
