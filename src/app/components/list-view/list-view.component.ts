import {Component, OnInit, ViewChild} from '@angular/core';
import {PersonService} from '../../services/person.service';
import {TreeService} from '../../services/tree.service';
import {Router} from '@angular/router';
import {EventService} from '../../services/event.service';
import {faCross, faGenderless, faMars, faVenus} from '@fortawesome/free-solid-svg-icons';
import {FamilyService} from '../../services/family.service';
import {Person} from '../../interfaces/api/models/person';
import {Sex} from '../../interfaces/api/enums/sex';
import {AbstractViewComponent} from '../abstract-view/abstract-view.component';
import {Pageable} from '../../interfaces/api/other/pageable';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent extends AbstractViewComponent implements OnInit {

  sexEnum = Sex;
  parents = new Map();
  crossIcon = faCross;
  femaleSexIcon = faVenus;
  maleSexIcon = faMars;
  undeterminedSexIcon = faGenderless;

  displayedColumns: string[] = ['photo', 'fullName', 'sex', 'lifespan', 'parents', 'options'];
  page: Pageable<Person> = new Pageable();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    protected personService: PersonService,
    protected eventService: EventService,
    public treeService: TreeService,
    private familyService: FamilyService,
    private router: Router,
    protected messageService: MessageService) {
    super(eventService, treeService, personService, messageService);
  }

  ngOnInit(): void {
    this.page.number = 0;
    this.page.size = 20;
    this.refreshData();
  }

  refreshData(event?: PageEvent) {
    this.lifespans = new Map();

    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.treeService.activeTree == null) {
      this.router.navigate(['/trees']);
      return;
    }

    this.fetchPersons(this.page).subscribe(
      response => {
        for (const person of this.persons) {
          this.fetchParents(person);
          this.page.fromResponse(response);
          // this.persons = response.content; todo #75 shouldn't this line be used?
          this.pageIndex = response.number;
          this.pageSize = response.size;
          this.length = response.totalElements;
        }
      }
    );
    return event;
  }

  private fetchParents(person: Person) {
    this.parents = new Map<number, string[]>();
    this.familyService.getFamiliesContainingChild(person.id).subscribe(
      data => {
        const family = data.content;
        const parents = [Person.fullName(family[0]?.husband), Person.fullName(family[0]?.wife)];
        this.parents.set(person.id, parents);
      },
      error => {
        this.messageService.displayError(error.error);
      }
    );
  }

  getCleanLifespan(id: number) {
    return this.lifespans.get(id).getCleanLifespan();
  }

  areLifespansLoaded(): boolean {
    return this.lifespans.size === this.persons.length;
  }

  areParentsLoaded() {
    return this.parents.size === this.persons.length;
  }
}
