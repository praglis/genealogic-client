import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {TreeService} from '../../services/tree.service';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  UtilService = UtilService;

  constructor(
    public userService: UserService,
    public treeService: TreeService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  logOut() {
    this.userService.logout();
    this.router.navigate(['/login']);
  }

  getActiveTreeName(): string {
    if (this.treeService.activeTree == null) {
      return 'none';
    } else {
      return '\'' + this.treeService.activeTree.name + '\'';
    }

  }

  getLoggedUserNameAndUsername() {
    if (this.userService.currentlyLoggedUser != null) {
      const user = this.userService.currentlyLoggedUser;
      if (user?.firstName?.length <= 0) {
        user.firstName = null;
      }
      if (user?.lastName?.length <= 0) {
        user.lastName = null;
      }
      if (user.firstName == null && user.lastName == null) {
        return user.username;
      }
      let name;
      if (user.firstName != null && user.lastName != null) {
        name = ` (${user.firstName} ${user.lastName})`;
      } else if (user.firstName == null) {
        name = ` (${user.lastName})`;
      } else {
        name = ` (${user.firstName})`;
      }
      return `${user.username}${name}`;
    }
  }
}
