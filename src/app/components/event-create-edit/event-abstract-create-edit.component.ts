import {Component, ViewChild} from '@angular/core';
import {EventModel} from '../../interfaces/api/models/event-model';
import {FormGroup} from '@angular/forms';
import {TreeService} from '../../services/tree.service';
import {Person} from '../../interfaces/api/models/person';
import {Place} from 'src/app/interfaces/api/models/place';
import {EventType} from 'src/app/interfaces/api/enums/event-type';
import {UtilService} from '../../services/util.service';
import {EventService} from '../../services/event.service';
import {MatSnackBarRef} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {PlaceFormComponent} from '../place-form/place-form.component';
import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {Family} from '../../interfaces/api/models/family';
import {FamilyService} from '../../services/family.service';
import {MessageService} from '../../services/message.service';
import {DateFormComponent} from '../date-form/date-form.component';

@Component({
  selector: 'app-event-abstract-create-edit',
  templateUrl: './event-abstract-create-edit.component.html',
  styleUrls: ['./event-abstract-create-edit.component.scss']
})
export abstract class EventAbstractCreateEditComponent {
// todo add to labels file #60
  private static CANNOT_CREATE_EVENT_WARNING_MESSAGE = 'You can only create family events for people who are part of at least one family as spouse.';
  @ViewChild('eventDateComponent') eventDateComponent: DateFormComponent;
  Person = Person;
  Family = Family;
  Place = Place;
  UtilService = UtilService;
  EventService = EventService;
  EventType = EventType;

  event: EventModel;
  eventForm: FormGroup;
  eventSubmitted = false;
  treePersons: Person[];
  mainParticipantFamilies: Family[];
  invalidParticipantForFamilyEvent = false;
  header: string;
  snackbarRef: MatSnackBarRef<any>;
  filteredPersons: Person[];
  selectedFamily: any = new Family();

  protected constructor(public treeService: TreeService,
                        public dialog: MatDialog,
                        protected eventService: EventService,
                        protected messageService: MessageService,
                        protected router: Router,
                        protected familyService: FamilyService
  ) {
  }

  onKey(value) {
    this.filteredPersons = this.search(value);
  }

  onParticipantOrTypeChange() {
    this?.snackbarRef?.dismiss();
    this.invalidParticipantForFamilyEvent = false;

    if (this.eventForm.controls.participants.value != null && !EventService.isEventTypePersonal(this.eventForm.controls.type.value)) {
      const mainParticipant: Person = this.eventForm.controls.participants.value;
      this.familyService.getFamiliesContainingSpouse(mainParticipant.id).subscribe(
        resp => {
          this.mainParticipantFamilies = resp.content;
          if (resp.totalElements < 1) {
            this.messageService.displayDismissibleWarning(EventAbstractCreateEditComponent.CANNOT_CREATE_EVENT_WARNING_MESSAGE);
            this.invalidParticipantForFamilyEvent = true;
          } else {
            this.selectedFamily = this.mainParticipantFamilies[0];
          }
        },
        error => this.messageService.displayError(error.error)
      );
    }
  }

  search(value: string) {
    const filter = value.toLowerCase();
    return this.treePersons.filter(option => Person.fullName(option).toLowerCase().startsWith(filter));
  }

  eventTypes(): Array<string> {
    return Object.keys(EventType).filter(value => EventType[value] !== EventType.BIRT && EventType[value] !== EventType.DEAT);
  }


  abstract convertToEventModel(): EventModel;

  submitForm() {
    this.eventSubmitted = true;
    this.eventDateComponent.isDateModelSubmitted = true;
    if (this.eventForm.invalid || this.eventDateComponent.dateForm.invalid || this.invalidParticipantForFamilyEvent) {
      this.eventSubmitted = false;
      return;
    }
    this.createOrUpdate().subscribe(
      event => {
        this.router.navigate(['/event', event.id]);
      },
      error => this.messageService.displayDismissibleError(error.error),
      () => {
        this.eventSubmitted = false;
      }
    );
  }

  abstract createOrUpdate(): Observable<EventModel>;

  openPlaceForm() {
    const dialogRef = this.dialog.open(PlaceFormComponent, {
      width: '1000px',
      height: '600px',
      data: this.convertToEventModel()
    });

    dialogRef.afterClosed().subscribe(result => {
      this.eventForm.controls.place.setValue(result);
    });
  }

  shouldTypeBeSelected(eventType: string) {
    if (this.event?.type != null) {
      return this.event.type === eventType;
    } else {
      return false;
    }
  }
}
