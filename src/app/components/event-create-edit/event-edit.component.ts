import {Component} from '@angular/core';
import {EventService} from '../../services/event.service';
import {TreeService} from '../../services/tree.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from '../../services/message.service';
import {DateService} from '../../services/date.service';
import {EventModel} from '../../interfaces/api/models/event-model';
import {FormBuilder, Validators} from '@angular/forms';
import {UtilService} from '../../services/util.service';
import {MatDialog} from '@angular/material/dialog';
import {EventAbstractCreateEditComponent} from './event-abstract-create-edit.component';
import {Observable} from 'rxjs';
import {FamilyService} from '../../services/family.service';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-abstract-create-edit.component.html',
  styleUrls: ['./event-abstract-create-edit.component.scss']
})
export class EventEditComponent extends EventAbstractCreateEditComponent {

  constructor(
    public treeService: TreeService,
    public dialog: MatDialog,
    public dateService: DateService,
    protected eventService: EventService,
    protected messageService: MessageService,
    protected router: Router,
    protected familyService: FamilyService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    super(treeService, dialog, eventService, messageService, router, familyService);
    this.header = 'Edit an event';

    this.event = new EventModel();
    this.route.paramMap.subscribe(params => {
      this.event.id = +params.get('id') !== 0 ? +params.get('id') : null;

      if (this.event.id !== null) {
        eventService.get(this.event.id).subscribe(
          event => {
            this.event = event;
            if (event?.participants != null && treeService.activeTree == null) {
              treeService.activeTree = event.participants[0].person.tree;
            }
            this.initEventForm();
          }, error => {
            this.messageService.displayError(error.error);
          }
        );
      }
    });
  }

  convertToEventModel(): EventModel {
    return {
      id: this.event.id,
      name: UtilService.convertEmptyStringToNull(this.eventForm.controls.name.value),
      isNameCustom: this.checkIfNameIsCustom(this.event.name, this.eventForm.controls.name.value, this.event.isNameCustom),
      cause: UtilService.convertEmptyStringToNull(this.eventForm.controls.cause.value),
      details: UtilService.convertEmptyStringToNull(this.eventForm.controls.details.value),
      owner: this.treeService.activeTree.owner,
      participants: this.event.participants,
      place: this.eventForm.controls.place.value,
      startDate: this.eventDateComponent.submitDateModel(),
      type: this.eventForm.controls.type.value
    };
  }

  checkIfNameIsCustom(oldName: string, newName: string, wasNameCustom: boolean): boolean {
    if (UtilService.convertEmptyStringToNull(newName) == null) {
      return false;
    }
    if (oldName === newName) {
      return wasNameCustom;
    } else {
      return true;
    }
  }

  createOrUpdate(): Observable<EventModel> {
    return this.eventService.update(this.convertToEventModel());
  }

  private initEventForm() {
    this.eventForm = this.formBuilder.group(
      {
        name: [this.event.name, Validators.maxLength(90)],
        details: [this.event.details, Validators.maxLength(90)],
        cause: [this.event.cause, Validators.maxLength(90)],
        date: [''],
        place: [this.event.place],
        type: [this.event.type],
        participants: [this.event.participants, Validators.required]
      });
  }
}
