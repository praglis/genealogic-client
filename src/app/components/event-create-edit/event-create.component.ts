import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TreeService} from '../../services/tree.service';
import {PersonService} from '../../services/person.service';
import {Person} from '../../interfaces/api/models/person';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EventModel} from '../../interfaces/api/models/event-model';
import {UtilService} from '../../services/util.service';
import {EventAbstractCreateEditComponent} from './event-abstract-create-edit.component';
import {EventService} from '../../services/event.service';
import {MatDialog} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {EventParticipant} from '../../interfaces/api/models/event-participant';
import {FamilyService} from '../../services/family.service';
import {Family} from '../../interfaces/api/models/family';
import {EventTypeReverse} from '../../interfaces/api/enums/event-type-reverse';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-event-create',
  templateUrl: './event-abstract-create-edit.component.html',
  styleUrls: ['./event-abstract-create-edit.component.scss']
})
export class EventCreateComponent extends EventAbstractCreateEditComponent {

  eventForm: FormGroup;

  constructor(
    public treeService: TreeService,
    public dialog: MatDialog,
    protected eventService: EventService,
    protected messageService: MessageService,
    protected router: Router,
    protected familyService: FamilyService,
    private activatedRoute: ActivatedRoute,
    private personService: PersonService,
    private formBuilder: FormBuilder) {
    super(treeService, dialog, eventService, messageService, router, familyService);
    this.header = 'Create an event';

    this.initEventForm();

    personService.getTreePersons(treeService.activeTree.id).subscribe(
      response => {
        this.treePersons = response.content;
        this.filteredPersons = this.treePersons;
      },
      error => messageService.displayDismissibleError(error.error)
    );
  }

  initEventForm() {
    this.eventForm = this.formBuilder.group(
      {
        name: [null, Validators.maxLength(90)],
        details: [null, Validators.maxLength(90)],
        cause: [null, Validators.maxLength(90)],
        date: [null],
        place: [null],
        type: [EventTypeReverse.Retirement],
        participants: [null, Validators.required]
      });
  }

  convertToEventModel(): EventModel {
    return {
      id: null,
      name: UtilService.convertEmptyStringToNull(this.eventForm.controls.name.value),
      isNameCustom: UtilService.convertEmptyStringToNull(this.eventForm.controls.name?.value) != null,
      cause: UtilService.convertEmptyStringToNull(this.eventForm.controls.cause.value),
      details: UtilService.convertEmptyStringToNull(this.eventForm.controls.details.value),
      owner: this.treeService.activeTree.owner,
      participants: this.prepareParticipants(),
      place: this.eventForm.controls.place.value,
      startDate: this.eventDateComponent.submitDateModel(),
      type: this.eventForm.controls.type.value
    };
  }

  createOrUpdate(): Observable<EventModel> {
    return this.eventService.create(this.convertToEventModel());
  }

  private prepareParticipants() {
    let participants: EventParticipant[];
    if (EventService.isEventTypePersonal(this.eventForm.controls.type.value)) {
      participants = [new EventParticipant(this.eventForm.controls.participants.value, null, 'MAIN')];
    } else {
      const mainParticipant = this.eventForm.controls.participants.value;

      let secondParticipant: Person;
      if (Family.hasExactlyOneSpouse(this.selectedFamily)) {
        secondParticipant = null;
      } else {
        secondParticipant = Family.getOtherSpouse(this.selectedFamily, mainParticipant);
      }

      participants = [new EventParticipant(mainParticipant, null, 'SPOU')];
      if (secondParticipant != null) {
        participants.push(new EventParticipant(secondParticipant, null, 'SPOU'));
      }
    }
    return participants;
  }
}
