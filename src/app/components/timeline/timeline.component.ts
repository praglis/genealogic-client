import {Component, OnInit, ViewChild} from '@angular/core';
import {EventService} from '../../services/event.service';
import {TreeService} from '../../services/tree.service';
import {FamilyService} from '../../services/family.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventModel} from '../../interfaces/api/models/event-model';
import {Place} from '../../interfaces/api/models/place';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {Pageable} from '../../interfaces/api/other/pageable';
import {EventType} from '../../interfaces/api/enums/event-type';
import {EventTypeReverse} from '../../interfaces/api/enums/event-type-reverse';
import {DateService} from '../../services/date.service';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export abstract class TimelineComponent implements OnInit {
  subjectId: number;

  events: EventModel[] = [];

  Place = Place;
  EventTypeReverse = EventTypeReverse;
  EventType = EventType;

  public subjectName = '';

  displayedColumns: string[] = ['name', 'startDate', 'place', 'type', 'options'];
  page: Pageable<EventModel> = new Pageable();
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  protected constructor(
    protected eventService: EventService,
    protected treeService: TreeService,
    protected familyService: FamilyService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected messageService: MessageService,
    protected dateService: DateService
  ) {
    this.route.paramMap.subscribe(params => {
      this.subjectId = +params.get('id') !== 0 ? +params.get('id') : null;
    });
  }

  abstract refreshData(event?: PageEvent);

  abstract navigateToEventCreation();

  ngOnInit(): void {
    this.page.number = 0;
    this.refreshData();
  }

  delete(id: number) {
    // this.error = null;
    this.eventService.delete(id).subscribe(() => {
      this.refreshData();
    }, error => {
      this.messageService.displayError(error.error);
    });
  }


  getEventDate(event: EventModel): string {
    if (event?.startDate == null) {
      return '?';
    }
    return this.dateService.convertDateToString(event.startDate.value, event.startDate.valueAccuracy, event.startDate.phrase);
  }
}
