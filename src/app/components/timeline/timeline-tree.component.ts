import {Component, OnInit} from '@angular/core';
import {TimelineComponent} from './timeline.component';
import {EventService} from '../../services/event.service';
import {TreeService} from '../../services/tree.service';
import {FamilyService} from '../../services/family.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PageEvent} from '@angular/material/paginator';
import {MessageService} from '../../services/message.service';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-timeline-tree',
  templateUrl: '../timeline/timeline.component.html',
  styleUrls: ['../timeline/timeline.component.scss']
})
export class TimelineTreeComponent extends TimelineComponent implements OnInit {

  constructor(
    public eventService: EventService,
    protected treeService: TreeService,
    protected familyService: FamilyService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected messageService: MessageService,
    protected dateService: DateService
  ) {
    super(eventService, treeService, familyService, router, route, messageService, dateService);
    this.subjectName = ` of ${treeService.activeTree.name}`;
  }

  ngOnInit(): void {
    this.page.size = 15;
    super.ngOnInit();
  }

  refreshData(event?: PageEvent) {
    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.treeService.activeTree == null && this.subjectId == null) {
      this.router.navigate(['/trees']);
      return;
    } else if (this.subjectId == null) {
      this.subjectId = this.treeService.activeTree.id;
    }

    this.treeService.getTimelineForTree(this.subjectId, this.page).subscribe(
      response => {
        if (response == null || response.totalElements <= 0) {
          this.router.navigate(['/view/list']);
        }
        this.page.fromResponse(response);
        this.events = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        this.length = response.totalElements;
      }, error => this.messageService.displayError(error.error)
    );
    return event;
  }


  navigateToEventCreation() {
    this.router.navigate(['/event/new']);
  }
}

