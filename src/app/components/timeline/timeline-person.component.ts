import {Component, Input, OnInit} from '@angular/core';
import {TimelineComponent} from './timeline.component';
import {PageEvent} from '@angular/material/paginator';
import {EventService} from '../../services/event.service';
import {TreeService} from '../../services/tree.service';
import {FamilyService} from '../../services/family.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PersonService} from '../../services/person.service';
import {Person} from '../../interfaces/api/models/person';
import {DateService} from '../../services/date.service';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-timeline-person',
  templateUrl: '../timeline/timeline.component.html',
  styleUrls: ['../timeline/timeline.component.scss']
})
export class TimelinePersonComponent extends TimelineComponent implements OnInit {

  @Input() person: Person;

  constructor(
    protected eventService: EventService,
    protected treeService: TreeService,
    protected familyService: FamilyService,
    protected router: Router,
    protected route: ActivatedRoute,
    private personService: PersonService,
    protected messageService: MessageService,
    protected dateService: DateService
  ) {
    super(eventService, treeService, familyService, router, route, messageService, dateService);
  }

  ngOnInit(): void {
    this.subjectId = this.person.id;
    this.page.size = 5;
    super.ngOnInit();
  }

  navigateToEventCreation() {
    this.router.navigate(['/event/new']);
  }

  refreshData(event?: PageEvent) {

    if (event) {
      this.page.number = event.pageIndex;
      this.page.size = event.pageSize;
    }

    if (this.subjectId == null) {
      this.router.navigate(['/view/list']);
      return;
    }

    this.personService.getTimelineForPerson(this.subjectId, this.page).subscribe(
      response => {
        this.page.fromResponse(response);
        this.events = response.content;
        this.pageIndex = response.number;
        this.pageSize = response.size;
        this.length = response.totalElements;
      }, error => this.messageService.displayError(error.error)
    );
    return event;
  }

}
