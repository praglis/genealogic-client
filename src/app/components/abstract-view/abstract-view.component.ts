import {Component, OnInit} from '@angular/core';
import {Person} from '../../interfaces/api/models/person';
import {EventModel} from '../../interfaces/api/models/event-model';
import {EventService} from '../../services/event.service';
import {PersonService} from '../../services/person.service';
import {TreeService} from '../../services/tree.service';
import {tap} from 'rxjs/operators';
import {Lifespan} from '../../interfaces/chart-view/lifespan';
import {Relationship} from '../../interfaces/api/enums/relationship';
import {PageEvent} from '@angular/material/paginator';
import {Pageable} from '../../interfaces/api/other/pageable';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-abstract-view',
  templateUrl: './abstract-view.component.html',
  styleUrls: ['./abstract-view.component.scss']
})
export abstract class AbstractViewComponent implements OnInit {
  PERSON_DELETION_SUCCESSFUL_MESSAGE = ' was successfully deleted from tree!'; // todo add to labels file #60
  Person = Person;

  persons: Person[];
  selectedPerson: Person;

  lifespans = new Map();

  protected constructor(
    protected eventService: EventService,
    protected treeService: TreeService,
    protected personService: PersonService,
    protected messageService: MessageService
  ) {
  }

  relationships(): Array<string> {
    const keys = Object.keys(Relationship);
    return keys.slice(keys.length / 2);
  }

  ngOnInit(): void {
  }

  isPersonAlive(id: number) {
    return (this.lifespans.get(id) as Lifespan).isPersonAlive;
  }

  abstract refreshData(event?: PageEvent);

  delete(id: number) {
    this.personService.delete(id).subscribe((deletedPerson: Person) => {
      this.messageService.displaySuccess(Person.fullName(deletedPerson) + this.PERSON_DELETION_SUCCESSFUL_MESSAGE);
      this.refreshData();
    }, error => {
      this.messageService.displayError(error.error);
    });
  }

  protected fetchPersons(page?: Pageable<Person>) {
    const treeId = this.treeService.activeTree.id;

    return this.personService.getTreePersons(treeId, page).pipe(
      tap(personPag => {
        this.persons = personPag.content;

        if (this.selectedPerson?.id != null) {
          this.selectedPerson = this.persons.find(value => value.id === this.selectedPerson.id);
        } else {
          this.selectedPerson = this.persons[0];
        }

        for (const person of personPag.content) {
          this.fetchLifespan(person);
        }
      })
    );
  }

  protected fetchLifespan(person: Person) {
    let lifespanEvents: EventModel[];
    this.eventService.getLifespanEvents(person.id).subscribe(
      data => {
        lifespanEvents = data;

        const lifespan = new Lifespan(lifespanEvents[0]?.startDate, lifespanEvents[0]?.place);

        if (lifespanEvents[1] != null) {
          lifespan.deathDate = lifespanEvents[1]?.startDate;
          lifespan.deathOrResidencyPlace = lifespanEvents[1]?.place;
          lifespan.isPersonAlive = false;
        } else {
          lifespan.deathDate = null;
          lifespan.deathOrResidencyPlace = person?.residency;
          lifespan.isPersonAlive = true;
        }
        this.lifespans.set(person.id, lifespan);
      },
      error => {
        this.messageService.displayError(error.error);
      }
    );
  }
}
