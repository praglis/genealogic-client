import {Component, OnInit} from '@angular/core';
import {EventService} from '../../services/event.service';
import {TreeService} from '../../services/tree.service';
import {FamilyService} from '../../services/family.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EventModel} from '../../interfaces/api/models/event-model';
import {DateService} from '../../services/date.service';
import {MessageService} from '../../services/message.service';
import {User} from '../../interfaces/api/models/user';
import {FormControl, Validators} from '@angular/forms';
import OrgChart from '@balkangraph/orgchart.js';
import tree = OrgChart.tree;

const NULL_RESPONSE_FROM_API = 'Null response from GeneaLogic API.';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {
  treeId: number;

  permittedUsers: User[] = [];

  displayedColumns: string[] = ['email', 'username', 'options'];

  userToRevoke: User = null;
  usernameFormControl = new FormControl('', [Validators.required, Validators.maxLength(48)]);

  public constructor(
    protected eventService: EventService,
    public treeService: TreeService,
    protected familyService: FamilyService,
    protected router: Router,
    protected route: ActivatedRoute,
    protected messageService: MessageService,
    protected dateService: DateService,
  ) {
  }

  ngOnInit(): void {
    this.refreshData();
  }

  onSubmit() {
    if (this.usernameFormControl.invalid) {
      return;
    }
    this.treeService.grantPermission(tree, this.usernameFormControl.value).subscribe(
      () => {
        this.refreshData();
      }, error => this.messageService.displayError(error.error)
    );

  }

  refreshData() {
    if (this.treeService.activeTree == null) {
      this.router.navigate(['/trees']);
      return;
    } else if (this.treeId == null) {
      this.treeId = this.treeService.activeTree.id;
    }

    this.treeService.getPermittedUsersForTree(this.treeService.activeTree.id).subscribe(
      permittedUsersResponse => {
        if (permittedUsersResponse == null) {
          this.messageService.displayError(NULL_RESPONSE_FROM_API);
          return;
        }

        this.permittedUsers = permittedUsersResponse;
      }, error => this.messageService.displayError(error.error)
    );

  }

  revokePermission(treeId: number, userId: number) {
    this.treeService.revokePermission(treeId, userId).subscribe(() => {
      this.refreshData();
    }, error => {
      this.messageService.displayError(error.error);
    });
  }


  getEventDate(event: EventModel): string {
    if (event?.startDate == null) {
      return '?';
    }
    return this.dateService.convertDateToString(event.startDate.value, event.startDate.valueAccuracy, event.startDate.phrase);
  }
}
