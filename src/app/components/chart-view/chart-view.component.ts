import {AfterContentChecked, Component, OnInit} from '@angular/core';
import {TreeService} from '../../services/tree.service';
import {PersonService} from '../../services/person.service';
import {FamilyService} from '../../services/family.service';
import {OneParentNode, PartnerNode, TreeNode, TwoParentNode} from '../../interfaces/chart-view/chart-nodes';
import OrgChart from '@balkangraph/orgchart.js';
import {Person} from '../../interfaces/api/models/person';
import {Family} from '../../interfaces/api/models/family';
import {AbstractViewComponent} from '../abstract-view/abstract-view.component';
import {EventService} from '../../services/event.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PageEvent} from '@angular/material/paginator';
import {forkJoin} from 'rxjs';
import {Pageable} from '../../interfaces/api/other/pageable';
import {MessageService} from '../../services/message.service';
import orientation = OrgChart.orientation;

declare var $: any;

@Component({
  selector: 'app-chart-view',
  templateUrl: './chart-view.component.html',
  styleUrls: ['./chart-view.component.scss']
})
export class ChartViewComponent extends AbstractViewComponent implements OnInit, AfterContentChecked {

  families: Family[];
  isAncestorChartSelected = 'true';
  private descendantNodes: TreeNode[];
  private ancestorNodes: TreeNode[];
  private ancestorChart: OrgChart;
  private descendantChart: OrgChart;
  areChartsLoaded = false;
  selectedPersonDuringDelete: Person;
  selectedPersonForAddingRelative: Person;

  constructor(
    protected treeService: TreeService,
    protected personService: PersonService,
    protected eventService: EventService,
    private familyService: FamilyService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    protected messageService: MessageService
  ) {
    super(eventService, treeService, personService, messageService);
    activatedRoute.paramMap.subscribe(
      params => {
        const param = +params.get('id');
        if (param != null && !isNaN(param) && param !== 0) {
          this.selectedPerson = new Person();
          this.selectedPerson.id = param;
        }
      }
    );
  }

  private static initChartTemplate() {
    OrgChart.templates.ana.link = '<path stroke-linejoin="round" stroke="#bc8f8f" stroke-width="1px" fill="none" d="{rounded}" />';

    OrgChart.templates.ana = Object.assign({}, OrgChart.templates.ana);
    OrgChart.templates.ana.size = [300, 270];
    OrgChart.templates.ana.node =
      '<rect x="0" y="0" rx="5" ry="5" height="270" width="300" fill="#98fb98" stroke-width="1" stroke="#98fb98"></rect>' +
      '<circle  cx="150" cy="70" fill="#f0fff0" r="60" stroke="#3a3a34" stroke-width="1"></circle>';

    OrgChart.templates.ana.field_0 = '<text overflow="hidden" width="280" ' +
      'style="font-size: 16px;" fill="#bc8f8f" x="150" y="160" text-anchor="middle" font-weight="bold">{val}</text>';
    OrgChart.templates.ana.field_1 = '<text overflow="hidden" width="280" style="font-size: 16px;" fill="#3a3a34" x="10" ' +
      'y="190" text-anchor="start" font-weight="bold">{val}</text>';
    OrgChart.templates.ana.field_2 = '<text overflow="hidden" width="280" style="font-size: 16px;" fill="#3a3a34" x="10" ' +
      'y="220" text-anchor="start" font-weight="bold">{val}</text>';

    OrgChart.templates.ana.img_0 =
      '<clipPath id="{randId}"><circle cx="150" cy="70" r="60"></circle></clipPath>' +
      '<image preserveAspectRatio="xMidYMid slice" clip-path="url(#{randId})" xlink:href="{val}" x="90" y="10"  width="120" height="120"></image>';

    OrgChart.templates.ana.nodeMenuButton =
      '<g style="cursor:pointer;"  data-ctrl-n-menu-id="{id}">' +
      '<rect x="0" y="0" fill="#00fb98" fill-opacity="0" width="35" height="22"></rect>' +
      '<circle cx="8" cy="11" r="3" fill="#bc8f8f"></circle>' +
      '<circle cx="16" cy="11" r="3" fill="#bc8f8f"></circle>' +
      '<circle cx="24" cy="11" r="3" fill="#bc8f8f"></circle></g>';

    OrgChart.templates.ana.minus = '<circle cx="15" cy="0" r="15" fill="#98fb98" stroke="#bc8f8f" stroke-width="1"></circle>'
      + '<line x1="5" y1="0" x2="25" y2="0" stroke-width="2" stroke="#bc8f8f"></line>';
    OrgChart.templates.ana.plus = '<circle cx="15" cy="0" r="15" fill="#98fb98" stroke="#bc8f8f" stroke-width="1"></circle>'
      + '<line x1="5" y1="0" x2="25" y2="0" stroke-width="2" stroke="#bc8f8f"></line>'
      + '<line x1="15" y1="-10" x2="15" y2="10" stroke-width="2" stroke="#bc8f8f"></line>';

    OrgChart.templates.ana.linkAdjuster = {
      fromX: 0, fromY: 0, toX: 0, toY: -13
    };
  }

  switchSelectedChart(isAncestorChartSelected: string) {
    this.isAncestorChartSelected = isAncestorChartSelected;
    this.loadSelectedChart(this.isAncestorChartSelected);
  }

  ngOnInit() {
    ChartViewComponent.initChartTemplate();

    this.ancestorChart = this.initOrgChart(orientation.bottom);
    this.descendantChart = this.initOrgChart(orientation.top);

    this.fetchData();
  }

  fetchData() {
    // this.error = null;

    const probandId = this.treeService.activeTree?.probandId;

    if (probandId != null && this.selectedPerson == null) {
      this.selectedPerson = new Person();
      this.selectedPerson.id = this.treeService.activeTree.probandId;
    }

    forkJoin([
      this.fetchPersons(),
      this.familyService.getTreeFamilies(this.treeService.activeTree.id)
    ]).subscribe(
      (personsAndFamilies: Array<Pageable<Person | Family>>) => {
        if (this.persons == null || this?.persons?.length <= 0) {
          this.router.navigate(['/view/list']);
        }

        this.families = personsAndFamilies[1].content as Family[];

        this.prepareCharts();
        this.loadSelectedChart(this.isAncestorChartSelected);
      }, error => {
        this.messageService.displayError(error.error);
      });
  }

  refreshData(event?: PageEvent) {
    this.fetchData();
  }

  ngAfterContentChecked(): void {
    if (!this.areChartsLoaded && this.ancestorChart != null && this.descendantChart != null
      && this.lifespans != null && this.persons != null && this.families != null
      && this.persons.length > 0
      && this.lifespans.size === this.persons.length) {
      this.areChartsLoaded = true;
      this.prepareCharts();
      this.loadSelectedChart(this.isAncestorChartSelected);
    }
  }

  exportSVG(person: Person) {
    if (this.isAncestorChartSelected) {
      this.ancestorChart.exportSVG(this.buildExportOptions('svg', person));
    } else {
      this.descendantChart.exportSVG(this.buildExportOptions('svg', person));
    }
  }

  private prepareCharts() {
    if (this.selectedPersonDuringDelete != null) {
      this.selectedPerson = this.selectedPersonDuringDelete;
      this.selectedPersonDuringDelete = null;
    }
    this.ancestorNodes = this.createAncestorsChart(this.selectedPerson);
    this.descendantNodes = this.createDescendantsChart(this.selectedPerson);
  }

  exportPNG(person: Person) {
    if (this.isAncestorChartSelected) {
      this.ancestorChart.exportPNG(this.buildExportOptions('png', person));
    } else {
      this.descendantChart.exportPNG(this.buildExportOptions('png', person));
    }
  }

  private createAncestorsChart(subjectPerson: Person): any[] {
    let treeNodes: TreeNode[] = [];
    treeNodes.push(new TreeNode(subjectPerson, this.lifespans.get(subjectPerson.id)));
    treeNodes = treeNodes.concat(this.getAncestors(subjectPerson));
    return treeNodes;
  }

  private createDescendantsChart(subjectPerson: Person): TreeNode[] {
    let treeNodes: TreeNode[] = [];
    treeNodes.push(new TreeNode(subjectPerson, this.lifespans.get(subjectPerson.id)));
    treeNodes = treeNodes.concat(this.getDescendants(subjectPerson));
    return treeNodes;
  }

  private getAncestors(subjectPerson: Person): TreeNode[] {
    let treeNodes: TreeNode[] = [];

    const familyChild = this.findFamilyWithPersonAsChild(subjectPerson);
    if (familyChild == null) {
      return treeNodes;
    }

    this.findAllSpousesExceptFirst(subjectPerson)
      .forEach(value => treeNodes.push(new PartnerNode(value, subjectPerson.id, this.lifespans.get(value.id))));

    if (familyChild.husband != null) {
      treeNodes.push(new OneParentNode(familyChild.husband, subjectPerson.id, this.lifespans.get(familyChild.husband.id)));
      treeNodes = treeNodes.concat(this.getAncestors(familyChild.husband));
    }
    if (familyChild.wife != null) {
      treeNodes.push(new OneParentNode(familyChild.wife, subjectPerson.id, this.lifespans.get(familyChild.wife.id)));
      treeNodes = treeNodes.concat(this.getAncestors(familyChild.wife));
    }
    return treeNodes;
  }

  private getDescendants(subjectPerson: Person): TreeNode[] {
    let treeNodes: TreeNode[] = [];

    const familySpouse = this.findFamilyWithPersonAsSpouse(subjectPerson);
    if (familySpouse == null) {
      return treeNodes;
    }

    let subjectPartner: Person = null;

    if (familySpouse.wife != null && familySpouse.wife.id !== subjectPerson.id) {
      subjectPartner = familySpouse.wife;
    } else if (familySpouse.husband != null && familySpouse.husband.id !== subjectPerson.id) {
      subjectPartner = familySpouse.husband;
    }
    if (subjectPartner != null) {
      treeNodes.push(new PartnerNode(subjectPartner, subjectPerson.id, this.lifespans.get(subjectPartner.id)));

      if (familySpouse.children == null) {
        return treeNodes;
      }
      familySpouse.children.forEach(value => {
        treeNodes.push(new TwoParentNode(value, subjectPerson.id, subjectPartner.id, this.lifespans.get(value.id)));
        treeNodes = treeNodes.concat(this.getDescendants(value));
      });
      return treeNodes;
    } else {
      if (familySpouse.children == null) {
        return treeNodes;
      }
      familySpouse.children.forEach(value => {
        treeNodes.push(new OneParentNode(value, subjectPerson.id, this.lifespans.get(value.id)));
        treeNodes = treeNodes.concat(this.getDescendants(value));
      });
      return treeNodes;
    }
  }

  private findFamilyWithPersonAsChild(person: Person): Family {
    return this.families.find(fam => fam.children.find(child => child.id === person.id));
  }

  private findFamilyWithPersonAsSpouse(person: Person): Family {
    return this.findAllFamiliesWithPersonAsSpouse(person).pop();
  }

  private findAllFamiliesWithPersonAsSpouse(person: Person): Family[] {
    return this.families.filter(value => value.husband?.id === person.id || value.wife?.id === person.id);
  }

  private findAllSpousesExceptFirst(subjectPerson: Person): Person[] {
    const spouses = this.findAllFamiliesWithPersonAsSpouse(subjectPerson).map(value2 => {
      if (value2?.husband != null && value2.husband.id !== subjectPerson.id) {
        return value2.husband;
      } else if (value2?.wife != null && value2.wife.id !== subjectPerson.id) {
        return value2.wife;
      }
    });

    spouses.shift();
    return spouses as unknown as Person[];
  }

  exportPDF(person: Person) {
    if (this.isAncestorChartSelected) {
      this.ancestorChart.exportPDF(this.buildExportOptions('pdf', person));
    } else {
      this.descendantChart.exportPDF(this.buildExportOptions('pdf', person));
    }
  }

  private initOrgChart(customOrientation: OrgChart.orientation): OrgChart {
    return new OrgChart(document.getElementById('tree'), {
      template: 'ana',
      nodeBinding: {
        field_0: 'fullName',
        field_1: 'birth',
        field_2: 'deathOrResidency',
        img_0: 'photoUrl'
      },
      orientation: customOrientation,
      nodeMenu: {
        setSubject: {
          text: 'See person\'s tree',
          icon: '',
          onClick: (id) => {
            this.selectedPerson = this.persons.find(value => value.id === id);
            this.prepareCharts();
            this.loadSelectedChart(this.isAncestorChartSelected);
          }
        },
        viewPerson: {
          text: 'View details',
          icon: '',
          onClick: (id) => {
            this.router.navigate(['/person', id]);
          }
        },
        addRelative: {
          text: 'Add relative',
          icon: '',
          onClick: (id) => {
            this.selectedPersonForAddingRelative = this.persons.find(value => value.id === id);
            $('#relationModal').modal('toggle');
          }
        },
        editPerson: {
          text: 'Edit person',
          icon: '',
          onClick: (id) => {
            this.router.navigate(['/person', id, 'update']);
          }
        },
        deletePerson: {
          text: 'Delete person',
          icon: '',
          onClick: (id) => {
            if (this.selectedPerson.id !== id) {
              this.selectedPersonDuringDelete = this.selectedPerson;
            }
            this.delete(id);
          }
        }
      },
      nodeMouseClick: OrgChart.action.none
    });
  }

  private loadSelectedChart(isAncestorChartSelected: string) {
    if (JSON.parse(isAncestorChartSelected)) {
      this.ancestorChart.load(this.ancestorNodes);
    } else {
      this.descendantChart.load(this.descendantNodes);
    }
  }

  private buildExportOptions(fileExtension: string, person: Person, chartType: string = null): any {
    if (chartType == null) {
      chartType = this.isAncestorChartSelected ? 'ancestors' : 'descendants';
    }
    return {
      filename: `${this.treeService.activeTree.name} - ${chartType} of ${Person.fullName(person)}.${fileExtension}`,
      expandChildren: true,
      nodeId: person.id
    };
  }
}
