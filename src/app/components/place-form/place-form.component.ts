import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Place} from '../../interfaces/api/models/place';
import {UtilService} from '../../services/util.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EventModel} from '../../interfaces/api/models/event-model';
import {EventType} from '../../interfaces/api/enums/event-type';

@Component({
  selector: 'app-place-form',
  templateUrl: './place-form.component.html',
  styleUrls: ['./place-form.component.scss']
})
export class PlaceFormComponent implements OnInit {

  UtilService = UtilService;
  EventType = EventType;
  place: Place;
  placeType: string;
  placeForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<PlaceFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: EventModel) {
    this.place = data?.place;
    this.placeType = data?.type;
    this.placeForm = formBuilder.group(
      {
        name: [this.place?.name, [Validators.maxLength(120), Validators.required]],
        address: [this.place?.address, Validators.maxLength(180)],
        city: [this.place?.city, Validators.maxLength(60)],
        state: [this.place?.state, Validators.maxLength(60)],
        postalCode: [this.place?.postalCode, Validators.maxLength(10)],
        country: [this.place?.country, Validators.maxLength(60)]
      });
  }

  public static convertToPlaceModel(placeForm: FormGroup, id: number): Place {
    return {
      id,
      city: placeForm.controls.city.value,
      country: placeForm.controls.country.value,
      address: placeForm.controls.address.value,
      name: placeForm.controls.name.value,
      postalCode: placeForm.controls.postalCode.value,
      state: placeForm.controls.state.value,
    };
  }

  ngOnInit(): void {
  }

  submitPlace() {
    this.place = PlaceFormComponent.convertToPlaceModel(this.placeForm, this.place?.id);
    this.dialogRef.close(this.place);
  }

}
