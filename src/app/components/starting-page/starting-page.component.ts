import {Component, OnInit} from '@angular/core';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-starting-page',
  templateUrl: './starting-page.component.html',
  styleUrls: ['./starting-page.component.scss']
})
export class StartingPageComponent implements OnInit {
  UtilService = UtilService;

  constructor() {
  }

  ngOnInit(): void {
  }

}
