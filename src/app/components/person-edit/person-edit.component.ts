import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DateFormComponent} from '../date-form/date-form.component';
import {PersonService} from '../../services/person.service';
import {EventService} from '../../services/event.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TreeService} from '../../services/tree.service';
import {UserService} from '../../services/user.service';
import {EventParticipantService} from '../../services/event-participant.service';
import {EventType} from '../../interfaces/api/enums/event-type';
import {Person} from '../../interfaces/api/models/person';
import {Relationship} from '../../interfaces/api/enums/relationship';
import {BasicEvents} from '../../interfaces/api/other/basic-events';
import {Place} from '../../interfaces/api/models/place';
import {EventParticipant} from '../../interfaces/api/models/event-participant';
import {EventModel} from '../../interfaces/api/models/event-model';
import {EventTypeReverse} from '../../interfaces/api/enums/event-type-reverse';
import {Tree} from '../../interfaces/api/models/tree';
import {FileModel} from '../../interfaces/api/other/file-model';
import {FileService} from '../../services/file.service';
import {UtilService} from '../../services/util.service';
import {PlaceFormComponent} from '../place-form/place-form.component';

@Component({
  selector: 'app-person-edit',
  templateUrl: './person-edit.component.html',
  styleUrls: ['./person-edit.component.scss']
})
export class PersonEditComponent implements AfterViewInit, OnInit {

  personForm: FormGroup;
  personSubmitted = false;
  placeForm: FormGroup;
  placeType: string;
  @ViewChild('birthDateComponent') birthDateComponent;
  @ViewChild('deathDateComponent') deathDateComponent;
  @ViewChild('burialDateComponent') burialDateComponent;
  @ViewChild('familySelectComponent') familySelectComponent;
  eventType = EventType;
  UtilService = UtilService;
  person: Person = new Person();
  relativeId: number;
  relationship: Relationship;
  error = '';
  title = 'Add new person';
  shouldInitDeceasedDates = false;
  basicEvents: BasicEvents;
  activeServices = 0;
  photo: File;


  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private personService: PersonService,
    public treeService: TreeService,
    private eventService: EventService,
    private router: Router,
    private route: ActivatedRoute,
    private eventParticipantService: EventParticipantService,
    private fileService: FileService
  ) {
    this.route.paramMap.subscribe(params => {
      this.relationship = Relationship[params.get('relation')?.toUpperCase()];
      this.person.id = +params.get('id') !== 0 ? +params.get('id') : null;
      this.relativeId = +params.get('relativeId') !== 0 ? +params.get('relativeId') : null;

      if (this.person.id !== null) {
        this.title = 'Update existing person';
        personService.get(this.person.id).subscribe(
          person => {
            this.person = person;
            this.initPersonForm();
            this.initPlaceAndDateForms();
            this.updateActiveTreeIfNecessary();
          },
          error => {
            this.error = error.error;
            this.initPersonForm();
          }
        );
      } else if (this.relationship == null) {
        this.initPersonForm();
      } else {
        personService.get(this.relativeId).subscribe(
          person => {
            this.title = `Add ${Relationship[this.relationship].toLowerCase()} of ${Person.fullName(person)}`;
            this.updateActiveTreeIfNecessary(person.tree);
          },
          error => {
            this.error = error.error;
          }
        );
        this.initPersonForm();
      }
    });

    this.updateActiveTreeIfNecessary();

    this.placeForm = formBuilder.group(
      {
        name: ['', [Validators.maxLength(120), Validators.required]],
        address: ['', Validators.maxLength(180)],
        city: ['', Validators.maxLength(60)],
        state: ['', Validators.maxLength(60)],
        postalCode: ['', Validators.maxLength(10)],
        country: ['', Validators.maxLength(60)]
      });
  }

  private updateActiveTreeIfNecessary(tree?: Tree) {
    if (this.treeService.activeTree == null) {
      if (tree != null) {
        this.treeService.activeTree = tree;
      } else if (this.person?.tree != null) {
        this.treeService.activeTree = this.person?.tree;
      }
    }
  }

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
  }

  set Photo(value) {
    this.photo = value.target.files.item(0);
  }

  initPlaceAndDateForms() {
    this.eventService.getBasicByPerson(this.person.id).subscribe(
      basicEvents => {
        this.basicEvents = basicEvents;
        this.birthDateComponent.dateModel = basicEvents.birth.startDate;
        this.personForm.controls.birthPlace.setValue(basicEvents.birth.place);
        if (basicEvents.death != null) {
          this.personForm.controls.status.setValue('deceased');
          this.deathDateComponent.dateModel = basicEvents?.death?.startDate;
          this.personForm.controls.deathPlace.setValue(basicEvents.death.place);
          this.personForm.controls.deathCause.setValue(basicEvents.death.cause);

          if (basicEvents.burial != null) {
            this.burialDateComponent.dateModel = basicEvents?.burial?.startDate;
            this.personForm.controls.burialPlace.setValue(basicEvents.burial.place);
          }
        }
      }
    );
  }

  createBasicEvent(dfc: DateFormComponent, type: EventType, place: Place, role: string, cause?: string) {
    this.eventService.create(this.convertToEventModel(
      dfc, type, place, cause)).subscribe(
      createdEvent => {
        this.activeServices++;
        console.log('createdEvent', createdEvent);
        this.eventParticipantService.create(new EventParticipant(this.person, createdEvent as EventModel, role)).subscribe(
          () => {
            if (--this.activeServices <= 0) {
              if (this.person?.id != null) {
                this.router.navigate(['/person', this.person.id]);
              } else {
                this.router.navigate(['/view/list']);
              }
            }
          }, error => {
            this.error = error;
          });
      }, error => {
        this.error = error.error;
      }
    );
  }

  updateBasicEvent(dfc: DateFormComponent, type: EventType, place: Place, id: number, cause?: string) {
    this.activeServices++;
    this.eventService.update(this.convertToEventModel(dfc, type, place, cause, id)).subscribe(
      () => {
        if (--this.activeServices <= 0) {
          this.router.navigate(['/person', this.person.id]);
        }
      }, error => {
        this.error = error.error;
      }
    );
  }

  updateOrCreateBasicEvents() {
    if (this.basicEvents == null || this.basicEvents?.birth === null) {
      this.createBasicEvent(this.birthDateComponent, EventType.BIRT, this.personForm.controls.birthPlace.value, 'CHIL');
    } else {
      this.updateBasicEvent(this.birthDateComponent, EventType.BIRT, this.personForm.controls.birthPlace.value, this.basicEvents.birth.id,
        null);
    }
    this.birthDateComponent.isEventSubmitted = true;

    if (!this.isPersonAlive()) {
      if (this.basicEvents == null || this.basicEvents?.death === null) {
        this.createBasicEvent(this.deathDateComponent, EventType.DEAT, this.personForm.controls.deathPlace.value, 'MAIN',
          this.personForm.controls.deathCause.value);
      } else {
        this.updateBasicEvent(this.deathDateComponent, EventType.DEAT, this.personForm.controls.deathPlace.value, this.basicEvents.death.id,
          this.personForm.controls.deathCause.value
        );
      }
      this.deathDateComponent.isEventSubmitted = true;

      if (this.personForm.controls.burialPlace.value != null || this.personForm.controls.burialDate.value != null) {
        if (this.basicEvents == null || this.basicEvents?.burial === null) {
          this.createBasicEvent(this.burialDateComponent, EventType.BURI, this.personForm.controls.burialPlace.value, 'MAIN',
            null);
        } else {
          this.updateBasicEvent(this.burialDateComponent, EventType.BURI, this.personForm.controls.burialPlace.value,
            this.basicEvents.burial.id, null);
        }
        this.burialDateComponent.isEventSubmitted = true;
      }
    } else {
      if (this.basicEvents != null) {
        this.eventService.deleteIfExists(this.basicEvents?.death?.id);
        this.eventService.deleteIfExists(this.basicEvents?.burial?.id);
      }
    }
  }

  submitPlace(placeType: string) {
    const place: Place = PlaceFormComponent.convertToPlaceModel(this.placeForm, this.getPlaceId(placeType));
    this.setPlaceInPersonForm(placeType, place);
  }

  setPlaceInPersonForm(placeType: any, setValue: Place) {
    switch (placeType) {
      case EventType.BIRT: {
        this.personForm.controls.birthPlace.setValue(setValue);
        break;
      }
      case 'Residence': {
        this.personForm.controls.residencePlace.setValue(setValue);
        break;
      }
      case EventType.DEAT: {
        this.personForm.controls.deathPlace.setValue(setValue);
        break;
      }
      case EventType.BURI: {
        this.personForm.controls.burialPlace.setValue(setValue);
        break;
      }
    }
  }

  getPlaceId(placeType: any): number {
    switch (placeType) {
      case EventType.BIRT: {
        return this?.basicEvents?.birth?.place?.id;
      }
      case 'Residence': {
        return this?.person?.residency?.id;
      }
      case EventType.DEAT: {
        return this?.basicEvents?.death?.place?.id;
      }
      case EventType.BURI: {
        return this?.basicEvents?.burial?.place?.id;
      }
    }
  }

  getPlaceFromPersonForm(placeType: any): Place {
    switch (placeType) {
      case EventType.BIRT: {
        return this.personForm.controls.birthPlace.value;
      }
      case 'Residence': {
        return this.personForm.controls.residencePlace.value;
      }
      case EventType.DEAT: {
        return this.personForm.controls.deathPlace.value;
      }
      case EventType.BURI: {
        return this.personForm.controls.burialPlace.value;
      }
    }
  }

  toPlaceString(place: Place): string {
    if (place == null) {
      return '';
    }
    return `${place.name}`;
  }

  preparePlaceForm(eventType: any) {
    if (this.placeType === eventType) {
      return;
    }
    this.placeType = eventType;
    this.setPlaceForm(this.getPlaceFromPersonForm(eventType));
  }

  isPersonAlive() {
    return this.personForm.controls?.status.value !== 'deceased';
  }

  selectErrorHandler(valueEmitted) {
    this.error = valueEmitted.error;
  }

  private convertToEventModel(dfc: DateFormComponent, type: EventType, place: Place, cause: string, id?: number): EventModel {
    return {
      id,
      name: null,
      isNameCustom: false,
      place,
      startDate: dfc.submitDateModel(),
      type: EventTypeReverse[type],
      cause: UtilService.convertEmptyStringToNull(cause),
      details: null,
      owner: {id: this.userService.currentlyLoggedUser.id},
      participants: null
    };
  }

  private setPlaceForm(place: Place) {
    this.placeForm.controls.city.setValue(place?.city);
    this.placeForm.controls.country.setValue(place?.country);
    this.placeForm.controls.name.setValue(place?.name);
    this.placeForm.controls.postalCode.setValue(place?.postalCode);
    this.placeForm.controls.state.setValue(place?.state);
    this.placeForm.controls.address.setValue(place?.address);
  }

  submitForm() {
    this.personSubmitted = true;
    this.birthDateComponent.isDateModelSubmitted = true;
    this.deathDateComponent.isDateModelSubmitted = true;
    this.burialDateComponent.isDateModelSubmitted = true;
    this.error = '';
    if (this.personForm.invalid ||
      this.birthDateComponent.dateForm.invalid || this.deathDateComponent.dateForm.invalid || this.burialDateComponent.dateForm.invalid
    ) {
      return;
    }
    this.fileService.encodeFileInBase64(this.photo).subscribe(file => {
      if (this.person.id == null && this.relationship == null) {
        this.personService.create(this.convertToPersonModel(file)).subscribe(
          createdPerson => {
            this.person = createdPerson as Person;
            this.updateOrCreateBasicEvents();
          }, error => {
            this.error = error.error;
          }
        );
      } else if (this.relationship == null) {
        this.personService.update(this.convertToPersonModel(file)).subscribe(
          () => {
            this.updateOrCreateBasicEvents();
          },
          error => {
            this.error = error.error;
          }
        );
      } else {
        this.personService.createRelative(
          this.convertToPersonModel(file), this.relationship, this.relativeId, this.familySelectComponent.selectedFamily?.id).subscribe(
          createdPerson => {
            this.person = createdPerson as Person;
            this.updateOrCreateBasicEvents();
          }, error => {
            this.error = error.error;
          }
        );
      }
    });
  }

  private convertToPersonModel(file: FileModel): Person {
    return Person.of(
      this.person.id,
      this.treeService.activeTree != null ? this.treeService.activeTree : this.person.tree,
      UtilService.convertEmptyStringToNull(this.personForm.controls.namePrefix.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.firstName.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.middleName.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.nickname.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.surnamePrefix.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.lastName.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.maidenName.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.nameSuffix.value),
      this.personForm.controls.sex.value.toUpperCase(),
      this.personForm.controls.residencePlace.value,
      UtilService.convertEmptyStringToNull(this.personForm.controls.occupation.value),
      UtilService.convertEmptyStringToNull(this.personForm.controls.bio.value),
      file
    );
  }


  private initPersonForm() {
    this.personForm = this.formBuilder.group(
      {
        namePrefix: [this.person.namePrefix, Validators.maxLength(30)],
        firstName: [this.person.firstName, Validators.maxLength(60)],
        middleName: [this.person.middleName, Validators.maxLength(60)],
        nickname: [this.person.nickname, Validators.maxLength(30)],
        surnamePrefix: [this.person.surnamePrefix, Validators.maxLength(30)],
        lastName: [this.person.lastName, [Validators.maxLength(60), Validators.required]],
        maidenName: [this.person.maidenName, Validators.maxLength(60)],
        nameSuffix: [this.person.nameSuffix, Validators.maxLength(30)],
        sex: [this.person.sex != null ? this.person.sex.toLowerCase() : 'u'],
        status: ['living'],
        deathCause: [null, Validators.maxLength(90)],
        occupation: [this.person.occupation, Validators.maxLength(90)],
        bio: [this.person.bio, Validators.maxLength(10000)],
        birthDate: [''],
        deathDate: [''],
        burialDate: [''],
        birthPlace: [null],
        residencePlace: [this.person.residency],
        deathPlace: [null],
        burialPlace: [null],
      });
    this.photo = FileModel.toFile(this.person.photo);
  }
}
