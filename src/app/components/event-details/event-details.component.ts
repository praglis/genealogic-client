import {Component} from '@angular/core';
import {TreeService} from '../../services/tree.service';
import {EventService} from '../../services/event.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from '../../services/message.service';
import {EventModel} from '../../interfaces/api/models/event-model';
import {Place} from '../../interfaces/api/models/place';
import {EventType} from '../../interfaces/api/enums/event-type';
import {DateService} from '../../services/date.service';
import {EventTypeReverse} from '../../interfaces/api/enums/event-type-reverse';
import {Person} from '../../interfaces/api/models/person';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent {

  Person = Person;
  Place = Place;
  EventType = EventType;

  event: EventModel = new EventModel();

  constructor(
    private eventService: EventService,
    public treeService: TreeService,
    private router: Router,
    private route: ActivatedRoute,
    private messageService: MessageService,
    public dateService: DateService
  ) {
    this.route.paramMap.subscribe(params => {
      this.event.id = +params.get('id') !== 0 ? +params.get('id') : null;

      if (this.event.id !== null) {
        eventService.get(this.event.id).subscribe(
          event => {
            this.event = event;
            if (event?.participants != null && treeService.activeTree == null) {
              treeService.activeTree = event.participants[0].person.tree;
            }
          }, error => {
            this.messageService.displayError(error.error);
          }
        );
      }
    });
  }

  delete() {
    if (this.event.type === EventTypeReverse.Birth) {
      this.messageService.displayWarning('You can\'t delete birth event without deleting the person.');
      return;
    }
    this.eventService.delete(this.event.id).subscribe(() => {
      this.router.navigate(['/view/list']);
    }, error => this.messageService.displayError(error.error));
  }
}
