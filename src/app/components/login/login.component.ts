import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {first} from 'rxjs/operators';
import {LoginForm} from '../../interfaces/api/other/login-form';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('passwordField', {static: true}) passwordField: ElementRef;

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  UtilService = UtilService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {
    if (this.userService.currentlyLoggedUser) {
      this.router.navigate(['/about']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });
  }

  onSubmit() {
    this.submitted = true;
    const filledLoginForm = this.convertToLoginValues();

    this.loading = true;
    this.userService.login(filledLoginForm)
      .pipe(first())
      .subscribe(
        () => {
          this.router.navigate(['/about']);
        },
        error => {
          if (error.status < 400 || error.status >= 600) {
            this.error = 'Service is unavailable. Please try later.';
          } else {
            this.error = error.error;
          }
          this.loading = false;
        }
      );
  }

  convertToLoginValues(): LoginForm {
    return {
      email: this.registerForm.value.email,
      password: this.registerForm.value.password
    };
  }
}
