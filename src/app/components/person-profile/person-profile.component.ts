import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {EventType} from '../../interfaces/api/enums/event-type';
import {Person} from '../../interfaces/api/models/person';
import {BasicEvents} from '../../interfaces/api/other/basic-events';
import {UserService} from '../../services/user.service';
import {PersonService} from '../../services/person.service';
import {TreeService} from '../../services/tree.service';
import {EventService} from '../../services/event.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Place} from '../../interfaces/api/models/place';
import {faCross, faGenderless, faMars, faVenus} from '@fortawesome/free-solid-svg-icons';
import {Sex} from '../../interfaces/api/enums/sex';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {MessageService} from '../../services/message.service';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-person-profile',
  templateUrl: './person-profile.component.html',
  styleUrls: ['./person-profile.component.scss']
})
export class PersonProfileComponent {

  private static NEW_PROBAND_SET_MESSAGE = 'This person is new proband of the tree now.';// todo add to labels file #60
  Person = Person;
  Sex = Sex;
  eventType = EventType;
  person: Person = new Person();
  shouldInitDeceasedDates = false;
  basicEvents: BasicEvents;
  crossIcon = faCross;
  femaleSexIcon = faVenus;
  maleSexIcon = faMars;
  undeterminedSexIcon = faGenderless;
  photoUrl: SafeResourceUrl;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private personService: PersonService,
    public treeService: TreeService,
    private eventService: EventService,
    private router: Router,
    private route: ActivatedRoute,
    private domSanitizer: DomSanitizer,
    private messageService: MessageService,
    public dateService: DateService
  ) {
    this.route.paramMap.subscribe(params => {
      this.person.id = +params.get('id') !== 0 ? +params.get('id') : null;

      if (this.person.id !== null) {
        personService.get(this.person.id).subscribe(
          person => {
            this.person = person;
            this.photoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + person?.photo?.encodedBytes);
            treeService.activeTree = person.tree;
            eventService.getBasicByPerson(person.id).subscribe(
              basicEvents => {
                this.basicEvents = basicEvents;
              }, error => {
                this.messageService.displayError(error.error);
              }
            );
          }, error => {
            this.messageService.displayError(error.error);
          }
        );
      }
    });
  }

  isPersonAlive() {
    return this.basicEvents?.death == null;
  }

  selectErrorHandler(valueEmitted) {
    this.messageService.displayError(valueEmitted.error);
  }

  deletePerson() {
    this.personService.delete(this.person.id).subscribe(() => {
      this.router.navigate(['/view/list']);
    }, error => this.messageService.displayError(error.error));
  }

  toPlaceString(place: Place): string {
    if (place == null) {
      return 'unknown';
    }
    return Place.getFullPlaceString(place);
  }

  setPersonAsProband() {
    this.treeService.modifyProband(this.person.tree.id, this.person.id).subscribe(
      () => {
        this.treeService.reloadActiveTree();
        this.messageService.displaySuccess(PersonProfileComponent.NEW_PROBAND_SET_MESSAGE);
      }, error => this.messageService.displayError(error.error)
    );
  }
}
