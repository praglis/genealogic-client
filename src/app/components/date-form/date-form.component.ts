import {AfterViewInit, ChangeDetectorRef, Component, Input} from '@angular/core';
import {DateModel} from '../../interfaces/api/models/date-model';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {DateEstimation} from '../../interfaces/api/enums/date.estimation';
import {DateAccuracy} from '../../interfaces/api/enums/date-accuracy.enum';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-date-form',
  templateUrl: './date-form.component.html',
  styleUrls: ['./date-form.component.scss']
})
export class DateFormComponent implements AfterViewInit {

  isDateModelSubmitted = false;

  set dateModel(dateModel: DateModel) {
    if (dateModel == null) {
      return;
    }
    if (dateModel.phrase != null && dateModel.value == null) {
      this.dateForm.controls.dateValue.setValue(dateModel.phrase);
    } else {
      this.dateForm.controls.estimation.setValue(this.dateEstimationClass[dateModel.estimation] as unknown as DateEstimation);
      this.dateForm.controls.dateValue.setValue(
        this.dateService.convertDateToString(dateModel.value, dateModel.valueAccuracy, dateModel.phrase));
      this.dateForm.controls.maxDateValue.setValue(
        this.dateService.convertDateToString(dateModel.maxValue, dateModel.maxValueAccuracy, dateModel.phrase));
    }
  }

  @Input() dateType: string;
  @Input() date: DateModel;
  dateEstimationClass = DateEstimation;
  public dateForm: FormGroup;

  constructor(
    private dateService: DateService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef) {
    this.dateForm = formBuilder.group({
      estimation: [this.dateEstimationClass.EXACT],
      dateValue: ['', [dateValidator(), Validators.minLength(1), Validators.maxLength(10)]],
      maxDateValue: ['', [dateValidator(), Validators.minLength(1), Validators.maxLength(10)]]
    });
  }

  private static getAccuracyFromDateString(dateString: string): DateAccuracy {
    if (dateString == null || dateString === '') {
      return null;
    }
    const partsCount = dateString.split('.').length;
    if (partsCount < 1 || partsCount > 3) {
      return null;
    } else {
      return DateAccuracy[DateAccuracy[partsCount - 1]];
    }
  }

  ngAfterViewInit() {
    this.dateModel = this.date;
    this.cdr.detectChanges();
  }

  submitDateModel(): DateModel {
    if (this.dateForm.controls.dateValue.value == null && this.dateForm.controls.maxDateValue.value == null) {
      return null;
    } else {
      const dateModel1 = new DateModel(
        this.dateForm.controls.estimation.value,
        this.convertDateStringToDate(this.dateForm.controls.dateValue.value),
        DateFormComponent.getAccuracyFromDateString(this.dateForm.controls.dateValue.value),
        this.convertDateStringToDate(this.dateForm.controls.maxDateValue.value),
        DateFormComponent.getAccuracyFromDateString(this.dateForm.controls.maxDateValue.value)
      );
      console.log('DateFormComponent.returnDateModel#datemodel1', dateModel1);
      console.log('DateFormComponent.returnDateModel#this.convertDateStringToDate(this.dateForm.controls.dateValue.value)',
        this.convertDateStringToDate(this.dateForm.controls.dateValue.value));
      return dateModel1;
    }
  }

  convertDateStringToDate(text: string): Date {
    if (text == null || text === '') {
      return null;
    }
    const parts = text.split('.');
    const date = new Date();
    if (parts.length >= 1) {
      const year = Number(parts[parts.length - 1]) || new Date().getFullYear();
      date.setFullYear(year);
    }
    if (parts.length >= 2) {
      const month = Number(parts[parts.length - 2]) - 1 || 0;
      date.setMonth(month);
    }
    if (parts.length >= 3) {
      const day = Number(parts[parts.length - 3]) || 1;
      date.setDate(day);
    }
    return date;
  }
}

export function dateValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (control.value == null) {
      return null;
    }
    const datePieces: string[] = control.value.toString().split('.');
    if (datePieces.length > 3 || datePieces.length < 1) {
      return {dateValue: {value: control.value}, description: 'Too long or too short'};
    }
    if (datePieces.length >= 1) {
      const year = Number(datePieces[datePieces.length - 1]);
      if (isNaN(year) || year < -4713 || year > 9999) {
        return {dateValue: {value: control.value}, description: 'Invalid year'};
      }
    }
    if (datePieces.length >= 2) {
      const month = Number(datePieces[datePieces.length - 2]);
      if (isNaN(month) || month < 1 || month > 12) {
        return {dateValue: {value: control.value}, description: 'Invalid month'};
      }
    }
    if (datePieces.length >= 3) {
      const day = Number(datePieces[datePieces.length - 3]);
      if (isNaN(day) || day < 1 || day > 31) {
        return {dateValue: {value: control.value}, description: 'Invalid day'};
      }
    }
    return null;
  };
}
