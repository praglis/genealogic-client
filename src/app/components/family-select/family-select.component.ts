import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FamilyService} from '../../services/family.service';
import {Relationship} from '../../interfaces/api/enums/relationship';
import {Family} from '../../interfaces/api/models/family';
import {Person} from '../../interfaces/api/models/person';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-family-select',
  templateUrl: './family-select.component.html',
  styleUrls: ['./family-select.component.scss']
})
export class FamilySelectComponent implements OnInit {

  @Input() relationship: Relationship;
  @Input() relativeId: number;
  families: Family[] = null;
  selectedFamily: Family = null;
  familyClass = Family;
  @Output() errorEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor(private familyService: FamilyService) {
  }

  private static getFullNameOrUnknown(person: Person, relationshipName?: string) {
    if (relationshipName == null) {
      relationshipName = 'person';
    }
    return person != null ? Person.fullName(person) : 'unknown ' + relationshipName;
  }

  ngOnInit(): void {
    switch (this.relationship) {
      case Relationship.FATHER:
        this.getFamiliesForParentOrSiblingRelation().subscribe(() => {
          this.families = this.families.filter(value => value.husband == null);
          this.initSelection();
        });
        break;
      case Relationship.MOTHER:
        this.getFamiliesForParentOrSiblingRelation().subscribe(() => {
          this.families = this.families.filter(value => value.wife == null);
          this.initSelection();
        });
        break;
      case Relationship.SPOUSE:
        this.getFamiliesForSpouseOrChildRelation().subscribe(
          () => {
            this.families = this.families.filter(value => Family.hasExactlyOneSpouse(value));
            this.initSelection();
          }
        );
        break;
      case Relationship.CHILD:
        this.getFamiliesForSpouseOrChildRelation().subscribe(() => this.initSelection());
        break;
    }
  }

  getContextualFamilyName(family: Family, relationship: Relationship): string {
    switch (relationship) {
      case Relationship.FATHER:
        return 'husband of ' + FamilySelectComponent.getFullNameOrUnknown(family.wife);

      case Relationship.MOTHER:
        return 'wife of ' + FamilySelectComponent.getFullNameOrUnknown(family.husband);

      case Relationship.CHILD:
        if (family.husband?.id === this.relativeId) {
          return 'child with ' + FamilySelectComponent.getFullNameOrUnknown(family.wife, 'mother');
        } else if (family.wife?.id === this.relativeId) {
          return 'child with ' + FamilySelectComponent.getFullNameOrUnknown(family.husband, 'father');
        } else {
          return 'child with unknown person';
        }

      case Relationship.SPOUSE:
        let option = 'parent of';
        if (family?.children != null) {
          for (const child of family.children) {
            option = option.concat(` ${Person.fullName(child)},`);
          }
          return option.slice(0, -1);
        } else {
          return option.concat(' unknown children');
        }
    }
  }

  private initSelection() {
    if (this.families.length > 0) {
      this.selectedFamily = this.families[0];
      this.families.push(new Family());
    }
  }

  private getFamiliesForParentOrSiblingRelation() {
    return this.familyService.getFamiliesContainingChild(this.relativeId).pipe(
      tap(families => this.families = families.content, error => this.errorEmitter.emit(error))
    );
  }

  private getFamiliesForSpouseOrChildRelation() {
    return this.familyService.getFamiliesContainingSpouse(this.relativeId).pipe(
      tap(families => this.families = families.content, error => this.errorEmitter.emit(error))
    );
  }
}
