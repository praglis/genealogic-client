import {Component, OnInit} from '@angular/core';
import {WelcomePageSectionEnum} from '../../interfaces/welcome-page-section.enum';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.scss']
})
export class WelcomePageComponent implements OnInit {
  UtilService = UtilService;
  Section = WelcomePageSectionEnum;
  expandedSection = -1;

  constructor() {
  }

  expandSection(index: number) {
    this.expandedSection = index;
  }

  ngOnInit(): void {
  }

}
