<h1 class="text-center py-4 promotion-title">About {{UtilService.APP_NAME}}</h1>

<mat-card class="bg-primary">
  <span style="font-size: 19px">
    {{UtilService.APP_NAME}} is a genealogical application created to aid genealogists with their work. It
    allows creating and managing families, which in {{UtilService.APP_NAME}} are called <strong>trees</strong>, as well
    as people and events belonging to these trees. The first version of the application was created by Paweł Raglis
    within the engineer's thesis 'Family tree maker' from the Faculty of Computer Science on Białystok University of
    Technology and published as an open-source project.
  </span><br>
  <br>
  Below you can find detailed instructions on how to use the site. Just click on one of the sections to expand it.
</mat-card>

<mat-accordion class="example-headers-align mb-5">
  <mat-expansion-panel (opened)="expandSection(Section.NAVBAR)" [expanded]="expandedSection === Section.NAVBAR"
                       class="mt-5">
    <mat-expansion-panel-header>
      <mat-panel-title>Navigation</mat-panel-title>
      <mat-panel-description>
        Learn about navigating through the site
        <mat-icon>map</mat-icon>
      </mat-panel-description>
    </mat-expansion-panel-header>

    On the top of the page there is <strong>navigation bar</strong> which displays some useful information and, of
    course, allows navigating through site. It displays your current account username and currently selected tree
    &ndash; if none tree is selected, go to <strong>Trees</strong> page using navigation button, where you can select
    one (see <em>Trees</em> section). When a tree is selected, two more button will appear &ndash; <strong>View</strong>,
    which allows viewing the tree in desired way (see <em>Tree views</em> section) and <strong>Tree timeline</strong>,
    which will lead you to tree timeline (see <em>Timelines</em> section). Clicking on {{UtilService.APP_NAME}} logo
    will redirect you back to this page.

    <mat-action-row>
      <strong>Also check:</strong>
      <button (click)="expandSection(Section.TREES)" color="primary" mat-button>Trees</button>
      <button (click)="expandSection(Section.TREE_VIEWS)" color="primary" mat-button>Tree views</button>
      <button (click)="expandSection(Section.TIMELINES)" color="primary" mat-button>Timelines</button>
    </mat-action-row>
  </mat-expansion-panel>

  <mat-expansion-panel (opened)="expandSection(Section.TREES)" [expanded]="expandedSection === Section.TREES">
    <mat-expansion-panel-header>
      <mat-panel-title>Trees</mat-panel-title>
      <mat-panel-description>
        Trees, branches, families
        <mat-icon>spa</mat-icon>
      </mat-panel-description>
    </mat-expansion-panel-header>

    To access your trees use the <strong>Trees</strong> button located in the navigation bar (top of the screen). From
    there you will be able to create, edit and delete your trees. When creating multiple trees, remember that trees do
    not depend on each other and there is no possibility of merging them. There are two ways of creating a tree in
    {{UtilService.APP_NAME}}:
    <ol>
      <li>Creating an entirely new, empty tree</li>
      <li>Importing an existing tree from a GEDCOM file (only GEDCOM versions 5.5.1 and 5.5.5 are supported)</li>
    </ol>
    Both operations may be performed with the use of corresponding buttons.
    After you created your first tree you can view it (see <em>Tree views</em> section). Viewing the tree will select
    it as the <strong>current tree</strong>, which is displayed in the <strong>navigation bar</strong>.

    <mat-action-row>
      <strong>Also check:</strong>
      <button (click)="expandSection(Section.NAVBAR)" color="primary" mat-button>Navigation</button>
      <button (click)="expandSection(Section.TREE_VIEWS)" color="primary" mat-button>Tree views</button>
    </mat-action-row>
  </mat-expansion-panel>

  <mat-expansion-panel (opened)="expandSection(Section.TREE_VIEWS)" [expanded]="expandedSection === Section.TREE_VIEWS">
    <mat-expansion-panel-header>
      <mat-panel-title>Tree views</mat-panel-title>
      <mat-panel-description>
        Forms of viewing tree
        <mat-icon>chrome_reader_mode</mat-icon>
      </mat-panel-description>
    </mat-expansion-panel-header>

    {{UtilService.APP_NAME}} allows you to view your trees in two different ways. After creating new empty tree use
    <strong>list view</strong> to add first person to the tree.
    <br>
    <br>
    <h5>List view</h5>
    Idea behind <strong>list view</strong> is pretty simple &ndash; it contains list of people belonging to a tree.
    Buttons
    will allow you to manage people in the tree as well as view more detailed information about them in their
    <strong>profiles</strong> (see <em>Person's profile</em> section). List view provides only basic information about
    displayed people.
    <br>
    <br>
    <h5>Chart view</h5>
    Another type of tree view is <strong>chart view</strong>. It shows family centered around one main person who is
    called <strong>proband</strong>.
    It allows displaying tree chart in two modes:
    <ol>
      <li>Ancestor mode, which shows proband and theirs ancestors (parents, grandparents, etc.)</li>
      <li>Descendant mode, which shows proband, theirs partner and theirs descendants (children, grandchildren, etc.)
      </li>
    </ol>
    Use select in upper-right corner of chart to switch between modes. To access options similar to ones in <strong>list
    view</strong>, click three dots located in upper left corner of any chart node. Among these options is one allowing
    temporary change of proband and viewing tree from their perspective. In order to change proband permanently, go to
    <strong>person's profile</strong> (see <em>Person's profile</em> section).
    You can also export a chart as PNG, SVG or PDF using <strong>export as...</strong> button in chart view.


    <mat-action-row>
      <strong>Also check:</strong>
      <button (click)="expandSection(Section.PERSONS_PROFILE)" color="primary" mat-button>Person's profile</button>
    </mat-action-row>
  </mat-expansion-panel>

  <mat-expansion-panel (opened)="expandSection(Section.TIMELINES)" [expanded]="expandedSection === Section.TIMELINES">
    <mat-expansion-panel-header>
      <mat-panel-title>Timelines</mat-panel-title>
      <mat-panel-description>
        Family events in chronological order
        <mat-icon>date_range</mat-icon>
      </mat-panel-description>
    </mat-expansion-panel-header>
    <p>
      Timelines are structures that show events in chronological order. In {{UtilService.APP_NAME}} there are two types
      of timelines: <strong>tree timelines</strong> and <strong>personal timelines</strong>. Tree timelines contain all
      events which any person belonging to the tree participated in. Personal timelines show only events related to one
      specific person. You can access tree timeline through navigation bar while personal timeline can be found in
      person's profile (see <em>Person's profile</em> section).
    </p>
    <p>
      In both types of timelines, you can access details of events, edit them, or create new events. When creating
      events, remember that event type and participants are the two non-editable properties. Another fact worth noting
      is the status of birth and death events which are unique to a person and can be created/updated while creating or
      updating a person only.
    </p>

    <mat-action-row>
      <strong>Also check:</strong>
      <button (click)="expandSection(Section.PERSONS_PROFILE)" color="primary" mat-button>Person's profile</button>
    </mat-action-row>
  </mat-expansion-panel>

  <mat-expansion-panel (opened)="expandSection(Section.PERSONS_PROFILE)"
                       [expanded]="expandedSection === Section.PERSONS_PROFILE" class="mb-5">
    <mat-expansion-panel-header>
      <mat-panel-title>Person's profile</mat-panel-title>
      <mat-panel-description>
        Details about a person
        <mat-icon>account_circle</mat-icon>
      </mat-panel-description>
    </mat-expansion-panel-header>

    Profile is a place where you can see detailed info about a person. There are buttons allowing management of the
    person, including setting the person as proband of the tree (more about probands in <em>Tree views</em> section). If
    the person already is the proband, this fact will be displayed on the right side of person's name. Below the buttons
    person's timeline is located. It contains chronologically events related to the person (see <em>Timelines</em>
    section).

    <mat-action-row>
      <strong>Also check:</strong>
      <button (click)="expandSection(Section.TREE_VIEWS)" color="primary" mat-button>Tree views</button>
      <button (click)="expandSection(Section.TIMELINES)" color="primary" mat-button>Timelines</button>
    </mat-action-row>
  </mat-expansion-panel>
</mat-accordion>

