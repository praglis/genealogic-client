import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {first} from 'rxjs/operators';
import {RegistrationForm} from '../../interfaces/api/other/registration-form';
import {UtilService} from '../../services/util.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild('passwordField', {static: true}) passwordField: ElementRef;

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  UtilService = UtilService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) {
    if (this.userService.currentlyLoggedUser) {
      this.router.navigate(['/about']);
    }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.maxLength(48)]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(254)]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(80)]],
      repeatedPassword: ['', [
        Validators.required,
        repeatPasswordValidator(this.passwordField.nativeElement),
        Validators.minLength(8),
        Validators.maxLength(80)]],
      firstName: ['', Validators.maxLength(48)],
      lastName: ['', Validators.maxLength(48)]
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    const filledRegistrationForm = this.convertToRegistrationValues();

    this.loading = true;
    this.userService.register(filledRegistrationForm)
      .pipe(first())
      .subscribe(
        () => {
          this.router.navigate(['/login']);
        },
        error => {
          this.error = error.error;
          this.loading = false;
        }
      );
  }

  convertToRegistrationValues(): RegistrationForm {
    return {
      username: this.registerForm.value.username,
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      firstName: this.registerForm.value.firstName.length > 0 ? this.registerForm.value.firstName : null,
      lastName: this.registerForm.value.lastName.length > 0 ? this.registerForm.value.lastName : null,
      role: 'ROLE_USER'
    };
  }
}

export function repeatPasswordValidator(passwordElement: any): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const equals = passwordElement.value === control.value;
    return equals ? null : {matchingPassword: {value: control.value}};
  };
}

