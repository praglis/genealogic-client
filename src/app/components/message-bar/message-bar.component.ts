import {Component, Input} from '@angular/core';
import {Message} from '../../interfaces/message';

@Component({
  selector: 'app-message-bar',
  templateUrl: './message-bar.component.html',
  styleUrls: ['./message-bar.component.scss']
})
export class MessageBarComponent {

  @Input() message: Message = {body: '', type: ''};

  setMessage(body, type, time = 10000) {
    this.message.body = body;
    this.message.type = type;
    setTimeout(() => {
      this.message.body = '';
    }, time);
  }

  public setSuccessMessage(text: string) {
    this.setMessage(text, 'success');
  }

  public setErrorMessage(text: string) {
    this.setMessage(text, 'danger');
  }

}
