import {Component, OnInit} from '@angular/core';
import {TreeService} from '../../services/tree.service';
import {UserService} from '../../services/user.service';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {GedcomService} from '../../services/gedcom.service';
import {Router} from '@angular/router';
import {Tree} from '../../interfaces/api/models/tree';
import {SupportedGedcomVersion} from '../../interfaces/api/enums/supported-gedcom-version.enum';
import {saveAs} from 'file-saver';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-trees',
  templateUrl: './trees.component.html',
  styleUrls: ['./trees.component.scss']
})
export class TreesComponent implements OnInit {

  trees: Tree[];
  treeToDelete: Tree = null;
  editedTree: Tree = null;

  treeForm: FormGroup;
  submitted = false;

  GedcomFile: File = null;
  gedcomImportError = '';
  fileLoaded = false;
  loadingFile = false;

  SupportedGedcomVersions = SupportedGedcomVersion;

  constructor(
    private treeService: TreeService,
    private userService: UserService,
    private gedcomService: GedcomService,
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService
  ) {
  }

  set gedcomFile(value) {
    this.GedcomFile = value.target.files.item(0);
  }

  ngOnInit(): void {
    this.refreshData();
    this.treeForm = this.formBuilder.group({
      name: ['', [Validators.minLength(1), Validators.maxLength(50), treeNameValidator()]],
    });
  }

  refreshData() {
    this.treeToDelete = null;
    this.editedTree = null;
    this.fileLoaded = false;
    this.treeService.getUserTrees(this.userService.currentlyLoggedUser.id).subscribe(
      treePageable => {
        this.trees = treePageable.content;
      }, error => {
        this.messageService.displayError(error.error);
      }
    );
  }

  delete(treeToDelete: Tree) {
    const loadingSnackBar = this.messageService.displayDismissibleInfo(`Deleting ${treeToDelete.name}. It may take a while...`);
    this.treeService.deleteTree(treeToDelete.id).subscribe(() => {
      loadingSnackBar.dismiss();
      this.messageService.displayDismissibleSuccess(`'${treeToDelete.name}' family tree was successfuly deleted.`);
      this.refreshData();
    }, error => {
      loadingSnackBar.dismiss();
      this.messageService.displayError(error.error);
    });
  }

  getTreeName(tree: Tree) {
    if (tree != null) {
      return tree.name;
    } else {
      return null;
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.treeForm.invalid) {
      return;
    }
    if (this.editedTree == null) {
      this.treeService.createTree(this.convertFormToObject()).subscribe(
        () => {
          this.refreshData();
        }, error => this.messageService.displayError(error.error)
      );
    } else {
      this.treeService.modifyTree(this.convertFormToObject()).subscribe(
        () => {
          this.refreshData();
        }, error => this.messageService.displayError(error.error)
      );
    }
  }

  convertFormToObject(): Tree {
    const tree = new Tree();
    tree.id = this.editedTree?.id;
    tree.name = this.treeForm.value.name;
    tree.owner = {id: this.userService.currentlyLoggedUser.id};
    return tree;
  }

  getTreeNameForForm(editedTree: Tree) {
    if (editedTree != null) {
      return editedTree.name;
    } else {
      return null;
    }
  }

  setActiveTree(tree: Tree) {
    this.treeService.activeTree = tree;
  }

  submitGedcom() {
    this.loadingFile = true;
    this.gedcomImportError = '';
    console.log('GF', this.GedcomFile);
    if (this.GedcomFile == null) {
      this.gedcomImportError = 'Please select a file.';
      return;
    }

    this.gedcomService.importGedcom(this.GedcomFile).subscribe(
      response => {
        console.log('IMPORTANT gi response: ', response);
        this.loadingFile = false;
        this.fileLoaded = true;
        this.router.navigate(['/trees']);
      }, error => {
        this.loadingFile = false;
        console.log('GIE', error);
        this.gedcomImportError = error.error;
        console.log('this.gedcomImportError', this.gedcomImportError);
      }
    );
  }

  exportGedcom(tree: Tree, version: SupportedGedcomVersion) {
    this.gedcomService.exportGedcom(tree.id, version).subscribe(
      response => {
        const blob = new Blob([response], {type: 'text/plain'});
        saveAs(blob, tree.name + '-exported.ged');
      }, error => this.messageService.displayError(error.error));
  }
}

export function treeNameValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (control.value == null) {
      return {treeName: {value: control.value, description: 'Tree name is required.'}};
    }
    const treeName = (control.value as string).trim();
    if (treeName.length < 1) {
      return {treeName: {value: control.value, description: 'Name is too short (minimum 1 character).'}};
    }
    if (treeName.length > 50) {
      return {treeName: {value: control.value, description: 'Name is too long (maximum 50 characters).'}};
    }
    if ((control.value as string).includes(',')) {
      return {treeName: {value: control.value, description: 'Tree name can not contain commas.'}};
    }
    return null;
  };
}
