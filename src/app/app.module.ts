import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TreesComponent} from './components/trees/trees.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FooterComponent} from './components/footer/footer.component';
import {MainViewComponent} from './components/main-view/main-view.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ListViewComponent} from './components/list-view/list-view.component';
import {PersonEditComponent} from './components/person-edit/person-edit.component';
import {DateFormComponent} from './components/date-form/date-form.component';
import {DatePipe} from '@angular/common';
import {FamilySelectComponent} from './components/family-select/family-select.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ChartViewComponent} from './components/chart-view/chart-view.component';
import {MessageBarComponent} from './components/message-bar/message-bar.component';
import {PersonProfileComponent} from './components/person-profile/person-profile.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {TimelineComponent} from './components/timeline/timeline.component';
import {TreeService} from './services/tree.service';
import {WelcomePageComponent} from './components/welcome-page/welcome-page.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {StartingPageComponent} from './components/starting-page/starting-page.component';
import {MAT_SNACK_BAR_DATA, MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule, MatSnackBarRef} from '@angular/material/snack-bar';
import {EventDetailsComponent} from './components/event-details/event-details.component';
import {PlaceFormComponent} from './components/place-form/place-form.component';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {EventAbstractCreateEditComponent} from './components/event-create-edit/event-abstract-create-edit.component';
import {TimelineTreeComponent} from './components/timeline/timeline-tree.component';
import {TimelinePersonComponent} from './components/timeline/timeline-person.component';
import {EventEditComponent} from './components/event-create-edit/event-edit.component';
import {EventCreateComponent} from './components/event-create-edit/event-create.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {PermissionComponent} from './components/permission-edit/permission.component';

@NgModule({
  declarations: [
    AppComponent,
    TreesComponent,
    NavbarComponent,
    FooterComponent,
    MainViewComponent,
    LoginComponent,
    RegisterComponent,
    ListViewComponent,
    PersonEditComponent,
    DateFormComponent,
    FamilySelectComponent,
    ChartViewComponent,
    MessageBarComponent,
    PersonProfileComponent,
    TimelineTreeComponent,
    TimelinePersonComponent,
// @ts-ignore
    TimelineComponent,
    WelcomePageComponent,
    StartingPageComponent,
    EventDetailsComponent,
    EventEditComponent,
    PlaceFormComponent,
    EventCreateComponent,
// @ts-ignore
    EventAbstractCreateEditComponent,
    PermissionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatIconModule,
    MatCardModule,
    MatSnackBarModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule
  ],
  providers: [
    DatePipe, MessageBarComponent, TreeService,
    {provide: MatSnackBarRef, useValue: {}},
    {provide: MAT_SNACK_BAR_DATA, useValue: {}},
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 8000}},
    {provide: MatDialogRef, useValue: {}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
